package neu.edu.cs5200.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import neu.edu.cs5200.DAO.PropertyDescriptionDao;
import neu.edu.cs5200.entity.PropertyDescription;

@Service
public class PropertyDescriptionService {

	@Autowired
	private PropertyDescriptionDao propertyDescriptionDao;

	@Transactional
	public void addPropertyDescription(PropertyDescription propertyDescription) {
		propertyDescriptionDao.addPropertyDescription(propertyDescription);
	}

	@Transactional
	public List<PropertyDescription> listPropertyDescription() {

		return propertyDescriptionDao.listPropertyDescription();
	}
		
	@Transactional
	public void deletePropertyDescription(Long hotelId) {
		propertyDescriptionDao.deletePropertyDescription(hotelId);
	}

}
