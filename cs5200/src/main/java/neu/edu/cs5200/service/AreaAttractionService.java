package neu.edu.cs5200.service;

import neu.edu.cs5200.DAO.AreaAttractionDao;
import neu.edu.cs5200.entity.AreaAttraction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for Instantiating AreaAttraction DAO. TODO: May be this class
 * is not required.
 * 
 * @author vikas
 * 
 */
@Service
public class AreaAttractionService {
	@Autowired
	private AreaAttractionDao areaAttractionDao;

	@Transactional
	public AreaAttraction getAreaAttraction(Long hotelId) {
		return areaAttractionDao.getAreaAttraction(hotelId);
	}

	@Transactional
	public void deleteAreaAttraction(Long hotelId) {
		areaAttractionDao.deleteAreaAttraction(hotelId);
	}

	@Transactional
	public void updateAreaAttraction(AreaAttraction areaAttraction) {

		areaAttractionDao.updateAreaAttraction(areaAttraction);
	}

	@Transactional
	public void insertAreaAttraction(AreaAttraction areaAttraction) {
		areaAttractionDao.insertAreaAttraction(areaAttraction);
	}
}
