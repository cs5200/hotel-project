package neu.edu.cs5200.service;

import neu.edu.cs5200.DAO.WhatToExpectDescriptionDao;
import neu.edu.cs5200.entity.WhatToExpectDescription;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 * Service class for Instantiating WhatToExpectDescription DAO. 
 * 
 * @author priyanka
 * 
 */
@Service
public class WhatToExpectDescriptionService {
	@Autowired
	private WhatToExpectDescriptionDao whatToExpectDescriptionDao;
	
	@Transactional
	public WhatToExpectDescription getWhatToExpectDescription(Long hotelId) {
		
		return whatToExpectDescriptionDao.getWhatToExpectDescription(hotelId);
	}
	
	@Transactional
	public void deleteWhatToExpectDescription(Long hotelId) {
		whatToExpectDescriptionDao.deleteWhatToExpectDescription(hotelId);
	}
	
	@Transactional
	public void updateWhatToExpectDescription(WhatToExpectDescription whatToExpectDescription) {
		whatToExpectDescriptionDao.updateWhatToExpectDescription(whatToExpectDescription);
	}
	
	@Transactional
	public void insertWhatToExpectDescription(WhatToExpectDescription whatToExpectDescription) {
		whatToExpectDescriptionDao.insertWhatToExpectDescription(whatToExpectDescription);
	}

}
