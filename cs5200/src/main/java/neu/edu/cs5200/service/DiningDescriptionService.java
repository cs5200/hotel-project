package neu.edu.cs5200.service;

import neu.edu.cs5200.DAO.DiningDescriptionDao;
import neu.edu.cs5200.entity.DiningDescription;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 * Service class for Instantiating DiningDescription DAO. 
 * 
 * @author priyanka
 * 
 */

@Service
public class DiningDescriptionService {
	
	@Autowired
	private DiningDescriptionDao diningDescriptionDao;
	
	@Transactional
	public DiningDescription getDiningDescription(Long hotelId) {
		return diningDescriptionDao.getDiningDescription(hotelId);
	}
	
	@Transactional
	public void deleteDiningDescription(Long hotelId) {
		diningDescriptionDao.deleteDiningDescription(hotelId);
	}
	
	@Transactional
	public void updateDiningDescription(DiningDescription diningDescription) {
		diningDescriptionDao.updateDiningDescription(diningDescription);
	}
	
	@Transactional
	public void insertDiningDescription(DiningDescription diningDescription) {
		diningDescriptionDao.insertDiningDescription(diningDescription);
	}

}
