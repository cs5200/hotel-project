package neu.edu.cs5200.service;

import java.util.Collections;
import java.util.List;

import neu.edu.cs5200.DAO.CustomerDao;
import neu.edu.cs5200.DAO.ReservationDao;
import neu.edu.cs5200.entity.Customer;
import neu.edu.cs5200.entity.Reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for Instantiating Reservation DAO.
 * 
 * @author priyanka
 * 
 */

@Service
public class ReservationService {

	@Autowired
	private ReservationDao reservationDao;
	
	@Autowired
	private CustomerDao customerDao;

	@Transactional
	public Reservation getReservation(Long reservation_id) {
		return reservationDao.getReservation(reservation_id);
	}

	@Transactional
	public void deleteReservation(Long reservationId) {
		reservationDao.deleteReservation(reservationId);
	}

	@Transactional
	public void updateReservation(Long reservationId) {
		reservationDao.updateReservation(reservationId);
	}

	@Transactional
	public void insertReservation(Reservation reservation) {
		Customer customer = customerDao.getCustomerByEmailId(reservation.getEmailId());
		if(customer != null)
		{
			reservation.setCustomerId(customer.getCustomerId());
		}
		reservationDao.insertReservation(reservation);
	}

	@Transactional
	public List<Reservation> listReservations(String emailId) {
		Customer customer = customerDao.getCustomerByEmailId(emailId);
		if(customer == null || customer.getCustomerId() == null)
		{
			return Collections.emptyList();
		}
		
		return reservationDao.listReservations(customer.getCustomerId());
	}

}
