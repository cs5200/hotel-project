package neu.edu.cs5200.service;

import neu.edu.cs5200.DAO.ReviewsDao;
import neu.edu.cs5200.entity.Reviews;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for Instantiating Reviews DAO. 
 * 
 * @author priyanka
 * 
 */

@Service
public class ReviewsService {
	
	@Autowired
	private ReviewsDao reviewsDao;
	
	@Transactional
	public Reviews getReviews(Long review_number) {
		return reviewsDao.getReviews(review_number);
	}
	
	@Transactional
	public void deleteReviews(Long review_number) {
		reviewsDao.deleteReviews(review_number);
	}
	
	@Transactional
	public void insertReviews(Reviews reviews) {
		Long reviewCount = reviewsDao.getCountByHotelId(reviews.getHotelId());
		reviews.setNoOfVotes(reviewCount+1);
		reviewsDao.insertReviews(reviews);
	}

	@Transactional
	public Boolean isReviewed(Long reservationId) {
		
		return reviewsDao.isReviewed(reservationId);
	}
	
	@Transactional
	public Long getCountByHotelId(Long hotelId) {
		
		return reviewsDao.getCountByHotelId(hotelId);
	}

	

}
