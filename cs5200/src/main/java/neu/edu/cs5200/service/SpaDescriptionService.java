package neu.edu.cs5200.service;

import neu.edu.cs5200.DAO.SpaDescriptionDao;
import neu.edu.cs5200.entity.SpaDescription;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for Instantiating SpaDescription DAO. 
 * 
 * @author priyanka
 * 
 */
@Service
public class SpaDescriptionService {
	
	@Autowired
	private SpaDescriptionDao spaDescriptionDao;
	
	@Transactional
	public SpaDescription getSpaDescription(Long hotelId) {
		return spaDescriptionDao.getSpaDescription(hotelId);
	}
	
	@Transactional
	public void deleteSpaDescription(Long hotelId) {
		spaDescriptionDao.deleteSpaDescription(hotelId);
	}
	
	@Transactional
	public void updateSpaDescription(SpaDescription spaDescription) {
		spaDescriptionDao.updateSpaDescription(spaDescription);
	}
	
	@Transactional
	public void insertSpaDescription(SpaDescription spaDescription) {
		spaDescriptionDao.insertSpaDescription(spaDescription);
	}
}
