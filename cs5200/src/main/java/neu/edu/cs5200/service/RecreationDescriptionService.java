package neu.edu.cs5200.service;

import neu.edu.cs5200.DAO.RecreationDescriptionDao;
import neu.edu.cs5200.entity.RecreationDescription;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for Instantiating RecreationDescription DAO. TODO: May be this class
 * is not required.
 * 
 * @author vikas
 * 
 */
@Service
public class RecreationDescriptionService {
	@Autowired
	private RecreationDescriptionDao recreationDescriptionDao;

	@Transactional
	public RecreationDescription getRecreationDescription(Long hotelId) {

		return recreationDescriptionDao.getRecreationDescription(hotelId);
	}
	
	@Transactional
	public void deleteRecreationDescription(Long hotelId) {
		recreationDescriptionDao.deleteRecreationDescription(hotelId);
	}

	@Transactional
	public void updateRecreationDescription(RecreationDescription recreationDescription) {

		recreationDescriptionDao.updateRecreationDescription(recreationDescription);
	}

	@Transactional
	public void insertRecreationDescription(RecreationDescription recreationDescription) {
		recreationDescriptionDao.insertRecreationDescription(recreationDescription);
	}
}
