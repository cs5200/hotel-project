package neu.edu.cs5200.service;

import neu.edu.cs5200.DAO.PaymentDetailsDao;
import neu.edu.cs5200.entity.PaymentDetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for payment Details
 * 
 * @author vikas
 * 
 */
@Service
public class PaymentDetailsService {
	@Autowired
	private PaymentDetailsDao paymentDetailsDao;

	@Transactional
	public PaymentDetails getPaymentDetails(Long reservationId) {
		return paymentDetailsDao.getPaymentDetails(reservationId);
	}

	@Transactional
	public void insertPaymentDetails(PaymentDetails paymentDetails) {
		paymentDetailsDao.insertPaymentDetails(paymentDetails);
	}

}
