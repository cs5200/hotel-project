package neu.edu.cs5200.service;

import neu.edu.cs5200.DAO.CustomerDao;
import neu.edu.cs5200.entity.Customer;

import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for Instantiating Customer DAO. 
 * 
 * @author priyanka
 * 
 */

@Service
public class CustomerService {
	@Autowired
	private CustomerDao customerDao;
	BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
	
	public CustomerService()
	{
		textEncryptor.setPassword("123");
	}
	
	@Transactional
	public Customer getCustomer(Long customerId) {
		return customerDao.getCustomer(customerId);
	}
	
	@Transactional
	public Customer getCustomerByUsername(String emailId) {
		
	  Customer customer = customerDao.getCustomerByEmailId(emailId);	
	  return customer;
	}
	
	
	@Transactional
	public String getCustomerByEmailId(String emailId) {
		
	  Customer customer = customerDao.getCustomerByEmailId(emailId);
	  
	  if(customer == null)
	  {
		  return null;
	  }
	  return textEncryptor.decrypt(customer.getPassword());
	}
	
	@Transactional
	public void deleteCustomer(Long customerId) {
		customerDao.deleteCustomer(customerId);
	}
	
	/*@Transactional
	public void updateCustomer(Customer customer)  {
		customerDao.updateCustomer(customer);
	}*/
	
	@Transactional
	public void insertCustomer(Customer customer) {
		customer.setPassword(textEncryptor.encrypt(customer.getPassword()));
		customerDao.insertCustomer(customer);
	}

}
