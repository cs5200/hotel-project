package neu.edu.cs5200.service;
import java.util.List;

import neu.edu.cs5200.DAO.ActivePropertyDao;
import neu.edu.cs5200.entity.ActiveProperty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 * Service class for Instantiating ActiveProperty DAO.
 * TODO: May be this class is not required.
 * @author vikas
 *
 */
@Service
public class ActivePropertyService {
	@Autowired
    private ActivePropertyDao activePropertyDao;
     
    @Transactional
    public void addActiveProperty(ActiveProperty activeProperty) {
    	activePropertyDao.addActiveProperty(activeProperty);
    }
 
    @Transactional
    public List<ActiveProperty> listActiveProperty() {
         return activePropertyDao.listActiveProperty();
    }
    
    @Transactional
    public List<ActiveProperty> listCities(String city) {
         return activePropertyDao.listCities(city);
    }
    
    @Transactional
    public List<ActiveProperty> listDetails(String city, String country)
    {
         return activePropertyDao.listDetails(city, country);
    }
    
    @Transactional
	public ActiveProperty getActiveProperty(Long hotelId) {
		return activePropertyDao.getActiveProperty(hotelId);
	}
    
    @Transactional
   	public void updateActiveProperty(ActiveProperty activeProperty) {

   		activePropertyDao.updateActiveProperty(activeProperty);
   	}
    
    
    @Transactional
   	public void insertActiveProperty(ActiveProperty activeProperty) {
   		activePropertyDao.insertActiveProperty(activeProperty);
   	}
    
    @Transactional
   	public void deleteActiveProperty(Long hotelId) {
  		activePropertyDao.deleteActiveProperty(hotelId);
   	}
}
