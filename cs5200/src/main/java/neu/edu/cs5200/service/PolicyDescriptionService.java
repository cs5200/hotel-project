package neu.edu.cs5200.service;

import neu.edu.cs5200.DAO.PolicyDescriptionDao;
import neu.edu.cs5200.entity.PolicyDescription;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for Instantiating PolicyDescription DAO. TODO: May be this
 * class is not required.
 * 
 * @author vikas
 * 
 */
@Service
public class PolicyDescriptionService {
	@Autowired
	private PolicyDescriptionDao policyDescriptionDao;

	@Transactional
	public PolicyDescription getPolicyDescription(Long hotelId) {
		return policyDescriptionDao.getPolicyDescription(hotelId);
	}

	@Transactional
	public void deletePolicyDescription(Long hotelId) {
		policyDescriptionDao.deletePolicyDescription(hotelId);
	}

	@Transactional
	public void updatePolicyDescription(PolicyDescription policyDescription) {

		policyDescriptionDao.updatePolicyDescription(policyDescription);
	}

	@Transactional
	public void insertPolicyDescription(PolicyDescription policyDescription) {
		policyDescriptionDao.insertPolicyDescription(policyDescription);
	}
}
