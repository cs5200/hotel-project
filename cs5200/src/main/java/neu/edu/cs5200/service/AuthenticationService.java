package neu.edu.cs5200.service;

import java.util.Collection;
import java.util.List;

import neu.edu.cs5200.entity.Customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class AuthenticationService implements UserDetailsService {
	@Autowired
	private CustomerService customerService;

	@Override
	public UserDetails loadUserByUsername(final String username)
			throws UsernameNotFoundException {

		return new UserDetails() {

			private static final long serialVersionUID = 2059202961588104658L;

			@Override
			public boolean isEnabled() {
				return true;
			}

			@Override
			public boolean isCredentialsNonExpired() {
				return true;
			}

			@Override
			public boolean isAccountNonLocked() {
				return true;
			}

			@Override
			public boolean isAccountNonExpired() {
				return true;
			}

			@Override
			public String getUsername() {
				return username;
			}

			@Override
			public String getPassword() {
				return customerService.getCustomerByEmailId(username);

			}

			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				Customer customer = customerService
						.getCustomerByUsername(username);
				List<SimpleGrantedAuthority> auths = new java.util.ArrayList<SimpleGrantedAuthority>();
				String role = customer.getRole();
				auths.add(new SimpleGrantedAuthority(role));
				if ("ROLE_ADMIN".equals(role)) {
					auths.add(new SimpleGrantedAuthority("ROLE_USER"));
				}
				return auths;
			}
		};
	}
}
