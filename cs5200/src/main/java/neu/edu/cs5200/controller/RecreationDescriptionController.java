package neu.edu.cs5200.controller;
import neu.edu.cs5200.entity.RecreationDescription;
import neu.edu.cs5200.service.RecreationDescriptionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for instantiated Service for RecreationDescription. Has methods for
 * request mapping URL's.
 * 
 * @author vikas
 * 
 */
@Controller
public class RecreationDescriptionController {
	@Autowired
	private RecreationDescriptionService recreationDescriptionService;

	@RequestMapping(value = "/recreation-description", method = RequestMethod.GET)
	@ResponseBody
	public RecreationDescription getRecreationDescription(
			@RequestParam("hotelId") Long hotelId) {
		return recreationDescriptionService.getRecreationDescription(hotelId);
	}
	
	
	@RequestMapping(value = "/admin/create-rec-desc/", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public RecreationDescription createRecreationDescription(@RequestBody RecreationDescription recreationDescription) {
 
		recreationDescriptionService.insertRecreationDescription(recreationDescription);
		return recreationDescriptionService.getRecreationDescription(recreationDescription.getHotelId());
    }
	
	
	@RequestMapping(value = "/admin/delete-rec-desc/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public String deleteRecreationDescription(@RequestBody RecreationDescription recreationDescription,@PathVariable Long hotelId) {
 
		recreationDescriptionService.deleteRecreationDescription(hotelId);
		return "OK";
    }
	
	
	@RequestMapping(value = "/admin/update-rec-desc/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public RecreationDescription updateRecreationDescription(@RequestBody RecreationDescription recreationDescription, 
    		@PathVariable Long hotelId) {
 
		recreationDescriptionService.updateRecreationDescription(recreationDescription);
		return recreationDescriptionService.getRecreationDescription(hotelId);
    }
	
}
