package neu.edu.cs5200.controller;

import neu.edu.cs5200.entity.Reviews;
import neu.edu.cs5200.service.ReviewsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for instantiated Service for Reviews. Has methods for
 * request mapping URL's.
 * 
 * @author priyanka
 * 
 */

@Controller
public class ReviewsController {
	@Autowired
	private ReviewsService reviewsService;
	
	@RequestMapping(value = "/reviews", method = RequestMethod.GET)
	@ResponseBody
	public Reviews getReviews(
			@RequestParam("review_number") Long review_number) {
		return reviewsService.getReviews(review_number);
	}
	
	@RequestMapping(value = "/is-reviewed/{reservationId}", method = RequestMethod.GET)
	@ResponseBody
	public Boolean isReviewed(@PathVariable Long reservationId) {
		return reviewsService.isReviewed(reservationId);
	}
	
	@RequestMapping(value = "/create-reviews/", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public Reviews createReviews(@RequestBody Reviews reviews) {
 
		reviewsService.insertReviews(reviews);
		return reviewsService.getReviews(reviews.getReviewNumber());
    }
	
	@RequestMapping(value = "/delete-reviews/{review_number}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public String deleteReviews(@RequestBody Reviews reviews,@PathVariable Long review_number) {
 
		reviewsService.deleteReviews(review_number);
		return "OK";
    }
}
