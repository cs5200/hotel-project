package neu.edu.cs5200.controller;

import java.util.List;
import java.util.Map;

import neu.edu.cs5200.entity.ActiveProperty;
import neu.edu.cs5200.service.ActivePropertyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for instantiated Service for ActiveProperty. Has methods for
 * request mapping URL's.
 * 
 * @author vikas
 * 
 */
@Controller
public class ActivePropertyController {

	@Autowired
	private ActivePropertyService activePropertyService;

	@RequestMapping(value = "/active-property", method = RequestMethod.GET)
	public String listContacts(Map<String, Object> map) {

		map.put("activeProperty", new ActiveProperty());
		map.put("activePropertyList",
				activePropertyService.listActiveProperty());
		return "activeProperty";
	}

	@RequestMapping(value = "/active-property/get-cities/{cityName}", method = RequestMethod.GET)
	public List<ActiveProperty> listCities(Map<String, Object> map,
			@PathVariable String cityName) {

		return activePropertyService.listCities(cityName);

	}

	@RequestMapping(value = "/active-property/list-details", method = RequestMethod.GET)
	public List<ActiveProperty> listDetails(Map<String, Object> map,
			@RequestParam("city") String cityName,
			@RequestParam("country") String country) {

		return activePropertyService.listDetails(cityName, country);
	}
}
