package neu.edu.cs5200.controller;
import neu.edu.cs5200.entity.PolicyDescription;
import neu.edu.cs5200.service.PolicyDescriptionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for instantiated Service for PolicyDescription. Has methods for
 * request mapping URL's.
 * 
 * @author vikas
 * 
 */
@Controller
public class PolicyDescriptionController {
	@Autowired
	private PolicyDescriptionService policyDescriptionService;

	@RequestMapping(value = "/policy-description", method = RequestMethod.GET)
	@ResponseBody
	public PolicyDescription getPolicyDescription(
			@RequestParam("hotelId") Long hotelId) {
		return policyDescriptionService.getPolicyDescription(hotelId);
	}
	
	
	@RequestMapping(value = "/admin/create-pol-desc/", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public PolicyDescription createPolicyDescription(@RequestBody PolicyDescription policyDescription) {
 
		policyDescriptionService.insertPolicyDescription(policyDescription);
		return policyDescriptionService.getPolicyDescription(policyDescription.getHotelId());
    }
	
	
	@RequestMapping(value = "/admin/delete-pol-desc/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public String deleteAreaAttraction(@RequestBody PolicyDescription policyDescription,@PathVariable Long hotelId) {
 
		policyDescriptionService.deletePolicyDescription(hotelId);
		return "OK";
    }
	
	
	@RequestMapping(value = "/admin/update-pol-desc/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public PolicyDescription updatePolicyDescription(@RequestBody PolicyDescription policyDescription, 
    		@PathVariable Long hotelId) {
 
		policyDescriptionService.updatePolicyDescription(policyDescription);
		return policyDescriptionService.getPolicyDescription(hotelId);
    }
}
