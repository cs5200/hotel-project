package neu.edu.cs5200.controller;

import neu.edu.cs5200.entity.PaymentDetails;
import neu.edu.cs5200.service.PaymentDetailsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for instantiated Service for PaymentDescription. Has methods for
 * request mapping URL's.
 * 
 * @author vikas
 * 
 */
@Controller
public class PaymentDetailsController {

	@Autowired
	private PaymentDetailsService paymentDetailsService;

	@RequestMapping(value = "create-payment-details/", headers = { "Content-type=application/json" }, method = RequestMethod.POST)
	@ResponseBody
	public PaymentDetails createPaymentDetails(
			@RequestBody PaymentDetails paymentDetails) {

		paymentDetailsService.insertPaymentDetails(paymentDetails);
		return paymentDetailsService.getPaymentDetails(paymentDetails
				.getReservationId());
	}
}
