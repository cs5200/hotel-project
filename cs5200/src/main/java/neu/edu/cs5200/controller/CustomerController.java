package neu.edu.cs5200.controller;

import neu.edu.cs5200.entity.Customer;
import neu.edu.cs5200.service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for instantiated Service for DiningDescription. Has methods for
 * request mapping URL's.
 * 
 * @author priyanka
 * 
 */

@Controller
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/customer", method = RequestMethod.GET)
	@ResponseBody
	public Customer getCustomer(@RequestParam("customerId") Long customerId) {
		return customerService.getCustomer(customerId);
	}

	@RequestMapping(value = "/get-customer", method = RequestMethod.GET)
	@ResponseBody
	public Customer getCustomerByUser() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth == null) {
			return new Customer();
		}
		String emailId = auth.getName();
		return customerService.getCustomerByUsername(emailId);
	}

	@RequestMapping(value = "/create-customer/", headers = { "Content-type=application/json" }, method = RequestMethod.POST)
	@ResponseBody
	public Customer createCustomer(@RequestBody Customer customer) {

		customerService.insertCustomer(customer);
		return customerService.getCustomer(customer.getCustomerId());
	}

	/*
	 * @RequestMapping(value = "/admin/update-customer/{customerId}", headers =
	 * {"Content-type=application/json"}, method = RequestMethod.POST)
	 * 
	 * @ResponseBody public Customer updateCustomer(@RequestBody Customer
	 * customer,
	 * 
	 * @PathVariable Long customerId) {
	 * 
	 * customerService.updateCustomer(customer); return
	 * customerService.getCustomer(customerId); }
	 */
}
