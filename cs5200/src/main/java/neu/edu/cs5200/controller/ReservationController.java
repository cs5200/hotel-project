package neu.edu.cs5200.controller;

import java.util.List;
import java.util.Map;

import neu.edu.cs5200.entity.Reservation;
import neu.edu.cs5200.service.ReservationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for instantiated Service for Reservation. Has methods for request
 * mapping URL's.
 * 
 * @author priyanka
 * 
 */
@Controller
public class ReservationController {
	@Autowired
	private ReservationService reservationService;

	@RequestMapping(value = "/reservation", method = RequestMethod.GET)
	@ResponseBody
	public Reservation getReservation(
			@RequestParam("reservationId") Long reservationId) {
		return reservationService.getReservation(reservationId);
	}

	@RequestMapping(value = "/get-history", method = RequestMethod.GET)
	@ResponseBody
	public List<Reservation> getHistory(Map<String, Object> map) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth == null) {
			return null;
		}
		String emailId = auth.getName();
		
		return reservationService.listReservations(emailId);
	}

	@RequestMapping(value = "/create-reservation/", headers = { "Content-type=application/json" }, method = RequestMethod.POST)
	@ResponseBody
	public Reservation createReservation(@RequestBody Reservation reservation) {

		reservationService.insertReservation(reservation);
		return reservationService
				.getReservation(reservation.getReservationId());
	}

	@RequestMapping(value = "/update-reservation/{reservationId}", headers = { "Content-type=application/json" }, method = RequestMethod.POST)
	@ResponseBody
	public Reservation updateReservation(@RequestBody Reservation reservation,
			@PathVariable Long reservationId) {

		reservationService.updateReservation(reservationId);
		return reservationService.getReservation(reservationId);
	}

}
