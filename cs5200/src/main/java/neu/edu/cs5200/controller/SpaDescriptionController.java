package neu.edu.cs5200.controller;

import neu.edu.cs5200.entity.SpaDescription;
import neu.edu.cs5200.service.SpaDescriptionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for instantiated Service for SpaDescription. Has methods for
 * request mapping URL's.
 * 
 * @author priyanka
 * 
 */
@Controller
public class SpaDescriptionController {
	@Autowired
	private SpaDescriptionService spaDescriptionService;
	
	@RequestMapping(value = "/spa-description", method = RequestMethod.GET)
	@ResponseBody
	public SpaDescription getSpaDescription(
			@RequestParam("hotelId") Long hotelId) {
		return spaDescriptionService.getSpaDescription(hotelId);
	}
	
	@RequestMapping(value = "/admin/create-spa-description/", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public SpaDescription createSpaDescription(@RequestBody SpaDescription spaDescription) {
 
		spaDescriptionService.insertSpaDescription(spaDescription);
		return spaDescriptionService.getSpaDescription(spaDescription.getHotelId());
    }
	
	@RequestMapping(value = "/admin/delete-spa-description/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public String deleteSpaDescription(@RequestBody SpaDescription spaDescription,@PathVariable Long hotelId) {
 
		spaDescriptionService.deleteSpaDescription(hotelId);
		return "OK";
    }
	
	@RequestMapping(value = "/admin/update-spa-description/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public SpaDescription updateSpaDescription(@RequestBody SpaDescription spaDescription, 
    		@PathVariable Long hotelId) {
 
		spaDescriptionService.updateSpaDescription(spaDescription);
		return spaDescriptionService.getSpaDescription(hotelId);
    }
}
