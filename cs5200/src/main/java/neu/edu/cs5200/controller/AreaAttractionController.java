package neu.edu.cs5200.controller;

import neu.edu.cs5200.entity.AreaAttraction;
import neu.edu.cs5200.service.AreaAttractionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for instantiated Service for AreaAttraction. Has methods for
 * request mapping URL's.
 * 
 * @author vikas
 * 
 */
@Controller
public class AreaAttractionController {
	@Autowired
	private AreaAttractionService areaAttractionService;

	@RequestMapping(value = "/area-attraction", method = RequestMethod.GET)
	@ResponseBody
	public AreaAttraction getAreaAttraction(
			@RequestParam("hotelId") Long hotelId) {
		return areaAttractionService.getAreaAttraction(hotelId);
	}

	@RequestMapping(value = "/admin/create-area-attraction/", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public AreaAttraction createAreaAttraction(@RequestBody AreaAttraction areaAttraction) {
 
		areaAttractionService.insertAreaAttraction(areaAttraction);
		return areaAttractionService.getAreaAttraction(areaAttraction.getHotelId());
    }
	
	
	@RequestMapping(value = "/admin/delete-area-attraction/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public String deleteAreaAttraction(@RequestBody AreaAttraction areaAttraction,@PathVariable Long hotelId) {
 
		areaAttractionService.deleteAreaAttraction(hotelId);
		return "OK";
    }
	
	@RequestMapping(value = "/admin/update-area-attraction/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public AreaAttraction updateAreaAttraction(@RequestBody AreaAttraction areaAttraction, 
    		@PathVariable Long hotelId) {
 
		areaAttractionService.updateAreaAttraction(areaAttraction);
		return areaAttractionService.getAreaAttraction(hotelId);
    }
}
