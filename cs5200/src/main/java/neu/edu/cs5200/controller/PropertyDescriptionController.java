package neu.edu.cs5200.controller;

import java.util.Map;

import neu.edu.cs5200.entity.PropertyDescription;
import neu.edu.cs5200.service.PropertyDescriptionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
/**
 * Controller class responsible for mapping property_description table.
 * @author vikas
 *
 */
@Controller
public class PropertyDescriptionController {
	@Autowired
	private PropertyDescriptionService propertyDescriptionService;

	@RequestMapping(value = "/property-description", method = RequestMethod.GET)
	public String listContacts(Map<String, Object> map) {

		map.put("propertyDescription", new PropertyDescription());
		map.put("PropertyDescriptionList",
				propertyDescriptionService.listPropertyDescription());
		return "activeProperty";
	}

}
