package neu.edu.cs5200.controller;

import neu.edu.cs5200.entity.DiningDescription;
import neu.edu.cs5200.service.DiningDescriptionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for instantiated Service for DiningDescription. Has methods for
 * request mapping URL's.
 * 
 * @author priyanka
 * 
 */
@Controller
public class DiningDescriptionController {

	@Autowired
	private DiningDescriptionService diningDescriptionService;
	
	@RequestMapping(value = "/dining-description", method = RequestMethod.GET)
	@ResponseBody
	public DiningDescription getDiningDescription(
			@RequestParam("hotelId") Long hotelId) {
		return diningDescriptionService.getDiningDescription(hotelId);
	}
	
	@RequestMapping(value = "/admin/create-dining-description/", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public DiningDescription createDiningDescription(@RequestBody DiningDescription diningDescription) {
 
		diningDescriptionService.insertDiningDescription(diningDescription);
		return diningDescriptionService.getDiningDescription(diningDescription.getHotelId());
    }
	
	@RequestMapping(value = "/admin/delete-dining-description/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public String deleteDiningDescription(@RequestBody DiningDescription diningDescription,@PathVariable Long hotelId) {
 
		diningDescriptionService.deleteDiningDescription(hotelId);
		return "OK";
    }
	
	@RequestMapping(value = "/admin/update-dining-description/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public DiningDescription updateDiningDescription(@RequestBody DiningDescription diningDescription, 
    		@PathVariable Long hotelId) {
 
		diningDescriptionService.updateDiningDescription(diningDescription);
		return diningDescriptionService.getDiningDescription(hotelId);
    }
	
}
