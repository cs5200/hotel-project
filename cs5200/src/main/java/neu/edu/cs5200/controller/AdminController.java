package neu.edu.cs5200.controller;


import neu.edu.cs5200.entity.ActiveProperty;
import neu.edu.cs5200.service.ActivePropertyService;
import neu.edu.cs5200.service.AreaAttractionService;
import neu.edu.cs5200.service.PolicyDescriptionService;
import neu.edu.cs5200.service.PropertyDescriptionService;
import neu.edu.cs5200.service.RecreationDescriptionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for instantiated Service for ActiveProperty.
 * Has methods for request mapping URL's.
 * @author vikas
 *
 */
@Controller
public class AdminController {
	@Autowired
	private ActivePropertyService activePropertyService;
	
	@Autowired
	private AreaAttractionService areaAttractionService;
	
	@Autowired
	private PolicyDescriptionService policyDescriptionService;
	
	@Autowired
	private PropertyDescriptionService propertyDescriptionService;
	
	@Autowired
	private RecreationDescriptionService recreationDescriptionService;
	
	@RequestMapping(value = "/admin/get-active-property/{hotelId}", method = RequestMethod.GET)
	@ResponseBody
	public ActiveProperty getActiveProperty(@PathVariable("hotelId") Long hotelId) {
		return activePropertyService.getActiveProperty(hotelId);
	}
	
	
	@RequestMapping(value = "/admin/update-active-property/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public ActiveProperty updateActiveProperty(@RequestBody ActiveProperty activeProperty, 
    		@PathVariable Long hotelId) {
 
		activePropertyService.updateActiveProperty(activeProperty);
		return activePropertyService.getActiveProperty(hotelId);
    }
	
	
	@RequestMapping(value = "/admin/create-active-property/", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public ActiveProperty createActiveProperty(@RequestBody ActiveProperty activeProperty) {
 
		activePropertyService.insertActiveProperty(activeProperty);
		return activePropertyService.getActiveProperty(activeProperty.getHotelId());
    }
	
	
	@RequestMapping(value = "/admin/delete-active-property/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public String deleteActiveProperty(@RequestBody ActiveProperty activeProperty,@PathVariable Long hotelId) {
 
		areaAttractionService.deleteAreaAttraction(hotelId);
		policyDescriptionService.deletePolicyDescription(hotelId);
		propertyDescriptionService.deletePropertyDescription(hotelId);
		recreationDescriptionService.deleteRecreationDescription(hotelId);
		activePropertyService.deleteActiveProperty(hotelId);
		
		return "OK";
    }
}
