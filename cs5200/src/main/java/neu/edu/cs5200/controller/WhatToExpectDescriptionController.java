package neu.edu.cs5200.controller;

import neu.edu.cs5200.entity.WhatToExpectDescription;
import neu.edu.cs5200.service.WhatToExpectDescriptionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller for instantiated Service for WhatToExpectDescription. Has methods for
 * request mapping URL's.
 * 
 * @author priyanka
 * 
 */

@Controller
public class WhatToExpectDescriptionController {
	@Autowired
	private WhatToExpectDescriptionService whatToExpectDescriptionService;
	
	@RequestMapping(value = "/what-to-expect-description", method = RequestMethod.GET)
	@ResponseBody
	public WhatToExpectDescription getWhatToExpectDescription(
			@RequestParam("hotelId") Long hotelId) {
		
		return whatToExpectDescriptionService.getWhatToExpectDescription(hotelId);
	}

	@RequestMapping(value = "/admin/create-what-to-expect-description/", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public WhatToExpectDescription createWhatToExpectDescription
    (@RequestBody WhatToExpectDescription whatToExpectDescription) {
		whatToExpectDescriptionService.insertWhatToExpectDescription(whatToExpectDescription);
		return whatToExpectDescriptionService.getWhatToExpectDescription(whatToExpectDescription.getHotelId());
    }
	
	@RequestMapping(value = "/admin/delete-what-to-expect-description/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public String deleteWhatToExpectDescription(@RequestBody WhatToExpectDescription whatToExpectDescription,
    		@PathVariable Long hotelId) {
 
		whatToExpectDescriptionService.deleteWhatToExpectDescription(hotelId);
		return "OK";
    }
	
	@RequestMapping(value = "/admin/update-what-to-expect-description/{hotelId}", 
			headers = {"Content-type=application/json"},
			method = RequestMethod.POST)
	@ResponseBody
    public WhatToExpectDescription updateWhatToExpectDescription
    (@RequestBody WhatToExpectDescription whatToExpectDescription, 
    		@PathVariable Long hotelId) {
 
		whatToExpectDescriptionService.updateWhatToExpectDescription(whatToExpectDescription);
		return whatToExpectDescriptionService.getWhatToExpectDescription(hotelId);
    }
}
