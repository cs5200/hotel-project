package neu.edu.cs5200.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "policy_description")
public class PolicyDescription implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "hotel_id", nullable = false)
	private Long hotelId;

	@Column(name = "language_code", nullable = true)
	private String languageCode;

	@Column(name = "pol_desc", nullable = true)
	private String polDesc;

	public PolicyDescription() {
		super();
	}

	public PolicyDescription(Long hotelId, String languageCode,
			String polDesc) {
		super();
		this.hotelId = hotelId;
		this.languageCode = languageCode;
		this.polDesc = polDesc;
	}


	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getPolDesc() {
		return polDesc;
	}

	public void setPolDesc(String polDesc) {
		this.polDesc = polDesc;
	}
}
