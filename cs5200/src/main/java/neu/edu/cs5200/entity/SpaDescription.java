package neu.edu.cs5200.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity for spa_description table.
 * 
 * @author priyanka
 * 
 */

@Entity
@Table(name = "spa_description")
public class SpaDescription implements Serializable {

	private static final long serialVersionUID = 795571837969809059L;
	
	@Id
	@Column(name = "hotel_id", nullable = false)
	private Long hotelId;

	@Column(name = "language_code", nullable = true)
	private String languageCode;
	
	@Column(name = "spa_desc", nullable = true)
	private String spaDesc;
	
	public SpaDescription() {
		super();
	}
	
	public SpaDescription(Long hotelId, String languageCode, 
			String spaDesc) {
		super();
		this.hotelId = hotelId;
		this.languageCode = languageCode;
		this.spaDesc = spaDesc;
	}

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getSpaDesc() {
		return spaDesc;
	}

	public void setSpaDesc(String spaDesc) {
		this.spaDesc = spaDesc;
	}

}
