package neu.edu.cs5200.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity for what_to_expect_description table.
 * 
 * @author priyanka
 * 
 */

@Entity
@Table(name = "what_to_expect_description")
public class WhatToExpectDescription implements Serializable{

	private static final long serialVersionUID = -8559960414732227402L;

	@Id
	@Column(name = "hotel_id", nullable = false)
	private Long hotelId;
	
	@Column(name = "language_code", nullable = true)
	private String languageCode;
	
	@Column(name = "what_to_expect", nullable = true)
	private String whatToExpect;
	
	public WhatToExpectDescription() {
		super();
	}
	
	public WhatToExpectDescription(Long hotelId, String languageCode,
			String whatToExpect) {
		super();
		this.hotelId = hotelId;
		this.languageCode = languageCode;
		this.whatToExpect = whatToExpect;
	}

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getWhatToExpect() {
		return whatToExpect;
	}

	public void setWhatToExpect(String whatToExpect) {
		this.whatToExpect = whatToExpect;
	}
}
