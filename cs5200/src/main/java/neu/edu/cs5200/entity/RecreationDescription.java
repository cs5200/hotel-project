package neu.edu.cs5200.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class mapping the recreation_description table
 * 
 * @author vikas
 * 
 */
@Entity
@Table(name = "recreation_description")
public class RecreationDescription implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "hotel_id", nullable = false)
	private Long hotelId;

	@Column(name = "language_code", nullable = true)
	private String languageCode;

	@Column(name = "rec_desc", nullable = true)
	private String recreationDesc;

	public RecreationDescription() {
		super();
	}

	public RecreationDescription(Long hotelId, String languageCode,
			String recreationDesc) {
		super();
		this.hotelId = hotelId;
		this.languageCode = languageCode;
		this.recreationDesc = recreationDesc;
	}

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getRecreationDesc() {
		return recreationDesc;
	}

	public void setRecreationDesc(String recreationDesc) {
		this.recreationDesc = recreationDesc;
	}
}
