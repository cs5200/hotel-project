package neu.edu.cs5200.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity for dining_description table.
 * 
 * @author priyanka
 * 
 */

@Entity
@Table(name = "dining_description")
public class DiningDescription implements Serializable{
	
	private static final long serialVersionUID = 6086132256345450452L;

	@Id
	@Column(name = "hotel_id", nullable = false)
	private Long hotelId;

	@Column(name = "language_code", nullable = true)
	private String languageCode;
	
	@Column(name = "dining_desc", nullable = true)
	private String diningDesc;
	
	public DiningDescription() {
		super();
	}

	public DiningDescription(Long hotelId, String languageCode,
			String diningDesc) {
		super();
		this.hotelId = hotelId;
		this.languageCode = languageCode;
		this.diningDesc = diningDesc;
	}

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getDiningDesc() {
		return diningDesc;
	}

	public void setDiningDesc(String diningDesc) {
		this.diningDesc = diningDesc;
	}
}
