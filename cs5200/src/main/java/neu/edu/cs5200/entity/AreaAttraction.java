package neu.edu.cs5200.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity for area_attraction table.
 * 
 * @author vikas
 * 
 */

@Entity
@Table(name = "area_attraction")
public class AreaAttraction implements Serializable {

	private static final long serialVersionUID = 8089584590777800572L;

	@Id
	@Column(name = "hotel_id", nullable = false)
	private Long hotelId;

	@Column(name = "language_code", nullable = true)
	private String languageCode;

	@Column(name = "area_attractions", nullable = true)
	private String areaAttractions;


	public AreaAttraction() {
		super();
	}

	public AreaAttraction(Long hotelId, String languageCode,
			String areaAttractions) {
		super();
		this.hotelId = hotelId;
		this.languageCode = languageCode;
		this.areaAttractions = areaAttractions;
	}

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getAreaAttractions() {
		return areaAttractions;
	}

	public void setAreaAttractions(String areaAttractions) {
		this.areaAttractions = areaAttractions;
	}

}
