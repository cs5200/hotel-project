package neu.edu.cs5200.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity for reservation table.
 * 
 * @author priyanka
 * 
 */

@Entity
@Table(name = "reservation")
public class Reservation implements Serializable{

	private static final long serialVersionUID = -36072440772759143L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "reservation_id", nullable = false)
	private Long reservationId;
	
	@Column(name = "customer_id", nullable = true)
	private Long customerId;
	
	@Column(name = "hotel_id", nullable = false)
	private Long hotelId;
	
	@Column(name = "checkin_date",  nullable = false)
	private String checkinDate;
	
	
	@Column(name = "rooms", nullable = false)
	private Integer rooms;
	
	@Column(name = "price", nullable = false)
	private Double price;
	
	@Column(name = "is_cancelled")
	private Boolean isCancelled;
	
	@Column(name = "first_name", nullable = false)
	private String firstName;
	
	@Column(name = "last_name",  nullable = false)
	private String lastName;
	
	@Column(name = "email_id", nullable = false)
	private String emailId;
	
	@Column(name = "phone_number", nullable = false)
	private String phoneNumber;
	
	@Column(name = "checkout_date",  nullable = false)
	private String checkoutDate;
	
	@Column(name = "adults",  nullable = false)
	private Integer adults;
	
	@Column(name = "children",  nullable = true)
	private Integer children;
	
	@Column(name = "bed_type",  nullable = true)
	private String bedType;
	
	public Reservation() {
		super();
	}



	public Reservation(Long reservationId, Long customerId, Long hotelId,
			String checkinDate, Integer rooms, Double price,
			Boolean isCancelled, String firstName, String lastName,
			String emailId, String phoneNumber, String checkoutDate,
			Integer adults, Integer children, String bedType) {
		super();
		this.reservationId = reservationId;
		this.customerId = customerId;
		this.hotelId = hotelId;
		this.checkinDate = checkinDate;
		this.rooms = rooms;
		this.price = price;
		this.isCancelled = isCancelled;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.phoneNumber = phoneNumber;
		this.checkoutDate = checkoutDate;
		this.adults = adults;
		this.children = children;
		this.bedType = bedType;
	}



	public Integer getAdults() {
		return adults;
	}



	public void setAdults(Integer adults) {
		this.adults = adults;
	}



	public Integer getChildren() {
		return children;
	}



	public void setChildren(Integer children) {
		this.children = children;
	}



	public String getBedType() {
		return bedType;
	}



	public void setBedType(String bedType) {
		this.bedType = bedType;
	}



	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public String getCheckinDate() {
		return checkinDate;
	}

	public void setCheckinDate(String checkinDate) {
		this.checkinDate = checkinDate;
	}

	public String getCheckoutDate() {
		return checkoutDate;
	}

	public void setCheckoutDate(String checkoutDate) {
		this.checkoutDate = checkoutDate;
	}

	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getIsCancelled() {
		return isCancelled;
	}

	public void setIsCancelled(Boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
}
