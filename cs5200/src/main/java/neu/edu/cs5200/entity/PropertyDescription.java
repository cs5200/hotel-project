package neu.edu.cs5200.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for mapping property_description table.
 * 
 * @author vikas
 * 
 */
@Entity
@Table(name = "property_description")
public class PropertyDescription implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "hotel_id", nullable = false)
	private Long hotelId;

	@Column(name = "language_code", nullable = true)
	private String languageCode;

	@Column(name = "property_category_desc", nullable = true)
	private String propertyCategoryDesc;

	//private ActiveProperty activeProperty;

	public PropertyDescription(Long hotelId, String languageCode,
			String propertyCategoryDesc) {
		this.hotelId = hotelId;
		this.languageCode = languageCode;
		this.propertyCategoryDesc = propertyCategoryDesc;
	}

	public PropertyDescription() {
		super();
	}
	/*

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hotel_id", nullable = false)
	public ActiveProperty getActiveProperty() {
		return this.activeProperty;
	}

	public void setActiveProperty(ActiveProperty activeProperty) {
		this.activeProperty = activeProperty;
	}*/

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getPropertyCategoryDesc() {
		return propertyCategoryDesc;
	}

	public void setPropertyCategoryDesc(String propertyCategoryDesc) {
		this.propertyCategoryDesc = propertyCategoryDesc;
	}

}
