package neu.edu.cs5200.entity;

import java.io.Serializable;

import javax.persistence.*;
/**
 * Entity for active_property table.
 * @author vikas
 *
 */

@Entity
@Table(name="active_property")
public class ActiveProperty implements Serializable{

	 
	private static final long serialVersionUID = 1L;

	   @Id
	   @GeneratedValue(strategy = GenerationType.AUTO)
	   @Column(name = "hotel_id")
	   private Long hotelId;

	   @Column(name = "sq_number", columnDefinition = "int default 1")
	   private Integer sqNumber=1;

	   @Column(name = "name")
	   private String name;

	   @Column(name = "address1")
	   private String address1;  
	   
	   @Column(name = "address2")
	   private String address2;  
	   
	   @Column(name = "city")
	   private String city;  
	   
	   
	   @Column(name = "state_province")
	   private String stateProvince;  
	   
	   @Column(name = "postal_code")
	   private String postalCode;  
	   
	   @Column(name = "country")
	   private String country;
	   
	   
	   @Column(name = "latitude")
	   private Double latitude;
	   
	   @Column(name = "longitude")
	   private Double longitude;
	   
	   @Column(name = "airport_code")
	   private String airportCode;
	   
	   @Column(name = "property_category")
	   private Integer propertyCategory;
	   
	   @Column(name = "property_currency")
	   private String propertyCurrency;
	   
	   @Column(name = "star_rating", nullable=true)
	   private Double starRating;
	   
	   @Column(name = "confidence")
	   private Integer confidence;
	   
	   @Column(name = "supplier_type")
	   private String supplierType;

	   @Column(name = "location")
	   private String location;

	   @Column(name = "chain_code_id")
	   private String chainCodeId;

	   @Column(name = "region_id")
	   private Integer regionId;

	   @Column(name = "high_rate")
	   private Double highRate;

	   @Column(name = "low_rate")
	   private Double lowRate;

	   @Column(name = "checkin_time")
	   private String checkinTime;

	   @Column(name = "checkout_time")
	   private String checkoutTime;

	   public ActiveProperty()
	   {
	     super();
	   }
	   
	public ActiveProperty(Long hotelId, Integer sqNumber, String name,
			String address1, String address2, String city,
			String stateProvince, String postalCode, String country,
			double latitude, Double longitude, String airportCode,
			int propertyCategory, String propertyCurrency, Double starRating,
			int confidence, String supplierType, String location,
			String chainCodeId, Integer regionId, Double highRate, Double lowRate,
			String checkinTime, String checkoutTime) {
	
		this.hotelId = hotelId;
		this.sqNumber = sqNumber;
		this.name = name;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.stateProvince = stateProvince;
		this.postalCode = postalCode;
		this.country = country;
		this.latitude = latitude;
		this.longitude = longitude;
		this.airportCode = airportCode;
		this.propertyCategory = propertyCategory;
		this.propertyCurrency = propertyCurrency;
		this.starRating = starRating;
		this.confidence = confidence;
		this.supplierType = supplierType;
		this.location = location;
		this.chainCodeId = chainCodeId;
		this.regionId = regionId;
		this.highRate = highRate;
		this.lowRate = lowRate;
		this.checkinTime = checkinTime;
		this.checkoutTime = checkoutTime;
	}

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public Integer getSqNumber() {
		return sqNumber;
	}

	public void setSqNumber(Integer sqNumber) {
		this.sqNumber = sqNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public Integer getPropertyCategory() {
		return propertyCategory;
	}

	public void setPropertyCategory(Integer propertyCategory) {
		this.propertyCategory = propertyCategory;
	}

	public String getPropertyCurrency() {
		return propertyCurrency;
	}

	public void setPropertyCurrency(String propertyCurrency) {
		this.propertyCurrency = propertyCurrency;
	}

	public Double getStarRating() {
		return starRating;
	}

	public void setStarRating(Double starRating) {
		this.starRating = starRating;
	}

	public Integer getConfidence() {
		return confidence;
	}

	public void setConfidence(Integer confidence) {
		this.confidence = confidence;
	}

	public String getSupplierType() {
		return supplierType;
	}

	public void setSupplierType(String supplierType) {
		this.supplierType = supplierType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getChainCodeId() {
		return chainCodeId;
	}

	public void setChainCodeId(String chainCodeId) {
		this.chainCodeId = chainCodeId;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public Double getHighRate() {
		return highRate;
	}

	public void setHighRate(Double highRate) {
		this.highRate = highRate;
	}

	public Double getLowRate() {
		return lowRate;
	}

	public void setLowRate(Double lowRate) {
		this.lowRate = lowRate;
	}

	public String getCheckinTime() {
		return checkinTime;
	}

	public void setCheckinTime(String checkinTime) {
		this.checkinTime = checkinTime;
	}

	public String getCheckoutTime() {
		return checkoutTime;
	}

	public void setCheckoutTime(String checkoutTime) {
		this.checkoutTime = checkoutTime;
	}	   
	   
}


