package neu.edu.cs5200.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity for reviews table.
 * 
 * @author priyanka
 * 
 */

@Entity
@Table(name = "reviews")
public class Reviews implements Serializable{

	private static final long serialVersionUID = 1191482296183460391L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "review_number", nullable = false)
	private Long reviewNumber;
	
	
	@Column(name = "review_date")
	private String reviewDate;
	
	@Column(name = "reservation_id", nullable = false)
	private Long reservationId;
	
	@Column(name = "review")
	private String review;
	
	@Column(name = "hotel_id")
	private Long hotelId;
	
	@Column(name = "no_of_votes")
	private Long noOfVotes;
	
	@Column(name = "rating")
	private Long rating;
	
	public Reviews() {
		super();
	}
	
	public Reviews(Long reviewNumber, String reviewDate
			, String review, Long noOfVotes, Long rating, Long reservationId,
			 Long hotelId) {
		super();
		this.reviewNumber = reviewNumber;
		this.reviewDate = reviewDate;
		this.reservationId = reservationId;
		this.review = review;
		this.noOfVotes = noOfVotes;
		this.rating = rating;
		this.hotelId = hotelId;
	}

	public Long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}

	public Long getReservationId() {
		return reservationId;
	}

	public Long getReviewNumber() {
		return reviewNumber;
	}

	public void setReviewNumber(Long reviewNumber) {
		this.reviewNumber = reviewNumber;
	}

	

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	public String getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(String reviewDate) {
		this.reviewDate = reviewDate;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public Long getNoOfVotes() {
		return noOfVotes;
	}

	public void setNoOfVotes(Long noOfVotes) {
		this.noOfVotes = noOfVotes;
	}

	public Long getRating() {
		return rating;
	}

	public void setRating(Long rating) {
		this.rating = rating;
	}
}
