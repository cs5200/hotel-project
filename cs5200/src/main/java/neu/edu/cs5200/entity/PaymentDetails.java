package neu.edu.cs5200.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity for payment details table.
 * 
 * @author Vikas
 * 
 */

@Entity
@Table(name = "payment_details")
public class PaymentDetails implements Serializable {

	private static final long serialVersionUID = 1107445208560239083L;

	@Id
	@Column(name = "reservation_id", nullable = false)
	private Long reservationId;

	@Column(name = "cardholder_name", nullable = false)
	private String cardholderName;

	@Column(name = "card_number", nullable = false)
	private String cardNumber;

	@Column(name = "exp_month", nullable = false)
	private String expMonth;

	@Column(name = "exp_year", nullable = false)
	private int expYear;

	@Column(name = "security_code", nullable = false)
	private int securityCode;

	@Column(name = "country", nullable = false)
	private String country;

	@Column(name = "email_address", nullable = false)
	private String emailAddress;

	@Column(name = "billing_address1", nullable = false)
	private String billingAddress1;

	@Column(name = "billing_address2")
	private String billingAddress2;

	@Column(name = "city", nullable = false)
	private String city;

	@Column(name = "postal_code", nullable = false)
	private String postalCode;

	public PaymentDetails() {
		super();
	}

	public PaymentDetails(Long reservationId, String cardholderName,
			String cardNumber, String expMonth, int expYear, int securityCode,
			String country, String emailAddress, String billingAddress1,
			String billingAddress2, String city, String postalCode) {
		super();
		this.reservationId = reservationId;
		this.cardholderName = cardholderName;
		this.cardNumber = cardNumber;
		this.expMonth = expMonth;
		this.expYear = expYear;
		this.securityCode = securityCode;
		this.country = country;
		this.emailAddress = emailAddress;
		this.billingAddress1 = billingAddress1;
		this.billingAddress2 = billingAddress2;
		this.city = city;
		this.postalCode = postalCode;
	}

	public Long getReservationId() {
		return reservationId;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	public String getCardholderName() {
		return cardholderName;
	}

	public void setCardholderName(String cardholderName) {
		this.cardholderName = cardholderName;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	public int getExpYear() {
		return expYear;
	}

	public void setExpYear(int expYear) {
		this.expYear = expYear;
	}

	public int getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(int securityCode) {
		this.securityCode = securityCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getBillingAddress1() {
		return billingAddress1;
	}

	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = billingAddress1;
	}

	public String getBillingAddress2() {
		return billingAddress2;
	}

	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = billingAddress2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

}
