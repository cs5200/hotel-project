package neu.edu.cs5200.DAO;

import java.util.List;


import neu.edu.cs5200.entity.Customer;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO for Customer
 * 
 * @author priyanka
 * 
 */

@Repository
public class CustomerDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public Customer getCustomer(Long customerId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Customer where customerId=:customerId");
		query.setParameter("customerId", customerId);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}	
		return (Customer) query.list().get(0);
	}
	
	public Customer getCustomerByEmailId(String emailId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Customer where emailId=:emailId");
		query.setParameter("emailId", emailId);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}	
		return (Customer) query.list().get(0);
	}
	
	public void deleteCustomer(Long customerId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from Customer where customerId=:customerId");
		
		query.setParameter("customerId", customerId);
		query.executeUpdate();
	}
	
	/*public void updateCustomer(Customer customer) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"update Customer set firstName=:firstName, lastName=:lastName, ssn=:ssn, " +
				"address=:address, city=:city, state=:state, country=:country, emailId=:emailId, " +
				"phoneNum=:phoneNum where customerId=:customerId");
		
		query.setParameter("firstName", customer.getFirstName());
		query.setParameter("lastName", customer.getLastName());
		query.setParameter("ssn", customer.getSsn());
		query.setParameter("address", customer.getAddress());
		query.setParameter("city", customer.getCity());
		query.setParameter("state", customer.getState());
		query.setParameter("country", customer.getCountry());
		query.setParameter("emailId", customer.getEmailId());
		query.setParameter("phoneNum", customer.getPhoneNum());
		
		query.executeUpdate();
	}*/
	
	public void insertCustomer(Customer customer) {
		sessionFactory.getCurrentSession().save(customer);
	}

}
