package neu.edu.cs5200.DAO;

import java.util.List;

import neu.edu.cs5200.entity.ActiveProperty;                                                                                                                                                                    

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO for ActiveProperty
 * 
 * @author vikas
 * 
 */
@Repository
public class ActivePropertyDao {
	@Autowired
	private SessionFactory sessionFactory;

	public void addActiveProperty(ActiveProperty activeProperty) {
		sessionFactory.getCurrentSession().save(activeProperty);
	}

	@SuppressWarnings("unchecked")
	public List<ActiveProperty> listActiveProperty() {
		return sessionFactory.getCurrentSession()
				.createQuery("from ActiveProperty").list();
	}

	@SuppressWarnings("unchecked")
	public List<ActiveProperty> listCities(String cityName) {
		Query query =  sessionFactory.getCurrentSession()
				.createQuery("from ActiveProperty where lower(city) like :cityName group by city");
		query.setParameter("cityName", cityName.toLowerCase()+"%");

		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<ActiveProperty> listDetails(String cityName, String country) {		
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from ActiveProperty where city=:cityName and country=:country and highRate > 0 order by starRating desc limit 30");
		query.setParameter("cityName", cityName);
		query.setParameter("country", country);

		return query.list();

	}
	
	
	public ActiveProperty getActiveProperty	(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from ActiveProperty where hotelId=:hotelId");
		query.setParameter("hotelId", hotelId);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}
		
		return (ActiveProperty) query.list().get(0);
	}
	
	
	public void updateActiveProperty(ActiveProperty activeProperty) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"update ActiveProperty set location=:location, name=:name, country=:country," +
				"sqNumber=:sqNumber, address1=:address1, address2=:address2," +
				"city=:city, latitude=:latitude, longitude=:longitude, regionId=:regionId," +
				"highRate=:highRate, lowRate=:lowRate, stateProvince=:stateProvince," +
				"postalCode=:postalCode, airportCode=:airportCode, propertyCategory=:propertyCategory," +
				"propertyCurrency=:propertyCurrency, starRating=:starRating, confidence=:confidence," +
				"supplierType=:supplierType, chainCodeId=:chainCodeId, checkinTime=:checkinTime," +
				"checkoutTime=:checkoutTime where hotelId=:hotelId");
		
		query.setParameter("hotelId", activeProperty.getHotelId());
		query.setParameter("name", activeProperty.getName());
		query.setParameter("sqNumber", activeProperty.getSqNumber());
		query.setParameter("location", activeProperty.getLocation());
		query.setParameter("country", activeProperty.getCountry());
		query.setParameter("address1", activeProperty.getAddress1());
		query.setParameter("address2", activeProperty.getAddress2());
		query.setParameter("city", activeProperty.getCity());
		query.setParameter("latitude", activeProperty.getLatitude());
		query.setParameter("longitude", activeProperty.getLongitude());
		query.setParameter("regionId", activeProperty.getRegionId());
		query.setParameter("highRate", activeProperty.getHighRate());
		query.setParameter("lowRate", activeProperty.getLowRate());
		query.setParameter("stateProvince", activeProperty.getStateProvince());
		query.setParameter("postalCode", activeProperty.getPostalCode());
		query.setParameter("airportCode", activeProperty.getAirportCode());
		query.setParameter("propertyCategory", activeProperty.getPropertyCategory());
		query.setParameter("propertyCurrency", activeProperty.getPropertyCurrency());
		query.setParameter("starRating", activeProperty.getStarRating());
		query.setParameter("confidence", activeProperty.getConfidence());
		query.setParameter("supplierType", activeProperty.getSupplierType());
		query.setParameter("chainCodeId", activeProperty.getChainCodeId());
		query.setParameter("checkinTime", activeProperty.getCheckinTime());
		query.setParameter("checkoutTime", activeProperty.getCheckoutTime());
		
		query.executeUpdate();
		//sessionFactory.getCurrentSession().getTransaction().commit();
		
	}
	
	public void insertActiveProperty(ActiveProperty activeProperty) {
			sessionFactory.getCurrentSession().save(activeProperty);
		//sessionFactory.getCurrentSession().getTransaction().commit();
		
	}
	
	public void deleteActiveProperty(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from ActiveProperty where hotelId=:hotelId");
		
		query.setParameter("hotelId", hotelId);
		query.executeUpdate();
	//sessionFactory.getCurrentSession().getTransaction().commit();
	
}
}
