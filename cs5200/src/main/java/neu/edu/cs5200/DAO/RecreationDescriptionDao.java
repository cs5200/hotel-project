package neu.edu.cs5200.DAO;

import java.util.List;

import neu.edu.cs5200.entity.RecreationDescription;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO for RecreationDescription
 * 
 * @author vikas
 * 
 */
@Repository
public class RecreationDescriptionDao {
	@Autowired
	private SessionFactory sessionFactory;

	public RecreationDescription getRecreationDescription(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from RecreationDescription where hotelId=:hotelId");
		query.setParameter("hotelId", hotelId);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}
		
		return (RecreationDescription) query.list().get(0);
	}
	
	
	public void deleteRecreationDescription(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from RecreationDescription where hotelId=:hotelId");
		
		query.setParameter("hotelId", hotelId);
		query.executeUpdate();
	}
	

	public void updateRecreationDescription(RecreationDescription recreationDescription) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"update RecreationDescription set recreationDesc=:recreationDesc," +
				"languageCode=:languageCode where hotelId=:hotelId");
		
		query.setParameter("hotelId", recreationDescription.getHotelId());
		query.setParameter("recreationDesc", recreationDescription.getRecreationDesc());
		query.setParameter("languageCode", "en_US");
		
		query.executeUpdate();
		//sessionFactory.getCurrentSession().getTransaction().commit();
	}
	
	public void insertRecreationDescription(RecreationDescription recreationDescription) {
			sessionFactory.getCurrentSession().save(recreationDescription);
		//sessionFactory.getCurrentSession().getTransaction().commit();
	}
}
