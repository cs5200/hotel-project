package neu.edu.cs5200.DAO;

import java.util.List;

import neu.edu.cs5200.entity.DiningDescription;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO for DiningDescription
 * 
 * @author priyanka
 * 
 */

@Repository
public class DiningDescriptionDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public DiningDescription getDiningDescription(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from DiningDescription where hotelId=:hotelId");
		query.setParameter("hotelId", hotelId);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}
		
		return (DiningDescription) query.list().get(0);
	}
	
	public void deleteDiningDescription(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from DiningDescription where hotelId=:hotelId");
		
		query.setParameter("hotelId", hotelId);
		query.executeUpdate();
	}
	
	public void updateDiningDescription(DiningDescription diningDescription) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"update DiningDescription set diningDesc=:diningDesc," +
				"languageCode=:languageCode where hotelId=:hotelId");
		
		query.setParameter("hotelId", diningDescription.getHotelId());
		query.setParameter("diningDesc", diningDescription.getDiningDesc());
		query.setParameter("languageCode", "en_US");
		
		query.executeUpdate();
	}
	
	public void insertDiningDescription(DiningDescription diningDescription) {
		sessionFactory.getCurrentSession().save(diningDescription);
	}

}
