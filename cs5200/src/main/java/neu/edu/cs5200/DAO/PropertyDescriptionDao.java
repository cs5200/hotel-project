package neu.edu.cs5200.DAO;

import java.util.List;

import neu.edu.cs5200.entity.PropertyDescription;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO class responsible for accessing property_description table.
 * @author vikas
 *
 */
@Repository
public class PropertyDescriptionDao {

	
	@Autowired
	private SessionFactory sessionFactory;

	public void addPropertyDescription(PropertyDescription propertyDescription) {
		sessionFactory.getCurrentSession().save(propertyDescription);
	}

	@SuppressWarnings("unchecked")
	public List<PropertyDescription> listPropertyDescription() {
		return sessionFactory.getCurrentSession()
				.createQuery("from PropertyDescription").list();
	}

	
	public void deletePropertyDescription(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from PropertyDescription where hotelId=:hotelId");
		
		query.setParameter("hotelId", hotelId);
		query.executeUpdate();
	}
	
	
	public void insertAreaAttraction(PropertyDescription propertyDescription) {
			sessionFactory.getCurrentSession().save(propertyDescription);
		//sessionFactory.getCurrentSession().getTransaction().commit();
	}
}
