package neu.edu.cs5200.DAO;

import java.util.Collections;
import java.util.List;

import neu.edu.cs5200.entity.Reservation;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO for Reservation
 * 
 * @author priyanka
 * 
 */

@Repository
public class ReservationDao {
	@Autowired
	private SessionFactory sessionFactory;

	public void insertReservation(Reservation reservation) {
		sessionFactory.getCurrentSession().save(reservation);
	}

	public void deleteReservation(Long reservationId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from Reservation where reservationId=:reservationId");

		query.setParameter("reservationId", reservationId);
		query.executeUpdate();
	}

	public void updateReservation(Long reservationId) {
		Query query = sessionFactory
				.getCurrentSession()
				.createQuery(
						"update Reservation set isCancelled=:isCancelled where reservationId=:reservationId");
		query.setParameter("isCancelled", true);
		query.setParameter("reservationId", reservationId);
		query.executeUpdate();
	}

	public Reservation getReservation(Long reservation_id) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Reservation where reservation_id=:reservation_id");
		query.setParameter("reservation_id", reservation_id);

		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}

		return (Reservation) query.list().get(0);
	}

	@SuppressWarnings("unchecked")
	public List<Reservation> listReservations(Long customerId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Reservation where customerId=:customerId order by checkinDate desc");
		query.setParameter("customerId", customerId);

		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return Collections.emptyList();
		}

		return query.list();
	}

}
