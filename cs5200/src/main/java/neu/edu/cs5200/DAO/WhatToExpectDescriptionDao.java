package neu.edu.cs5200.DAO;

import java.util.List;

import neu.edu.cs5200.entity.WhatToExpectDescription;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO for WhatToExpectDescription
 * 
 * @author priyanka
 * 
 */

@Repository
public class WhatToExpectDescriptionDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public WhatToExpectDescription getWhatToExpectDescription(Long hotelId){
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from WhatToExpectDescription where hotelId=:hotelId");
		query.setParameter("hotelId", hotelId);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}
		
		return (WhatToExpectDescription) query.list().get(0);	
	}
	
	public void deleteWhatToExpectDescription(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from WhatToExpectDescription where hotelId=:hotelId");
		query.setParameter("hotelId", hotelId);
		query.executeUpdate();
	}
	
	public void updateWhatToExpectDescription
	(WhatToExpectDescription whatToExpectDescription) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"update WhatToExpectDescription set whatToExpect=:whatToExpect," +
				"languageCode=:languageCode where hotelId=:hotelId");
		
		query.setParameter("hotelId", whatToExpectDescription.getHotelId());
		query.setParameter("whatToExpect", 
				whatToExpectDescription.getWhatToExpect());
		query.setParameter("languageCode", "en_US");
		query.executeUpdate();
	}
	
	public void insertWhatToExpectDescription
	(WhatToExpectDescription whatToExpectDescription) {
		sessionFactory.getCurrentSession().save(whatToExpectDescription);
	}
}
