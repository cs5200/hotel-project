package neu.edu.cs5200.DAO;

import java.util.List;

import neu.edu.cs5200.entity.SpaDescription;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
/**
 * DAO for SpaDescription
 * 
 * @author priyanka
 * 
 */

@Repository
public class SpaDescriptionDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public SpaDescription getSpaDescription(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from SpaDescription where hotelId=:hotelId");
		query.setParameter("hotelId", hotelId);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}
		
		return (SpaDescription) query.list().get(0);
	}
	
	public void deleteSpaDescription(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from SpaDescription where hotelId=:hotelId");
	
		query.setParameter("hotelId", hotelId);
		query.executeUpdate();
	}
	
	public void updateSpaDescription(SpaDescription spaDescription) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"update SpaDescription set spaDesc=:spaDesc," +
				"languageCode=:languageCode where hotelId=:hotelId");
		
		query.setParameter("hotelId", spaDescription.getHotelId());
		query.setParameter("spaDesc", spaDescription.getSpaDesc());
		query.setParameter("languageCode", "en_US");
		
		query.executeUpdate();
	}
	
	public void insertSpaDescription(SpaDescription spaDescription) {
		sessionFactory.getCurrentSession().save(spaDescription);
}

}
