package neu.edu.cs5200.DAO;

import java.util.List;

import neu.edu.cs5200.entity.PaymentDetails;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Repo for the paymentDetails. All payment information will be inserted through
 * this DAO
 * 
 * @author vikas
 * 
 */
@Repository
public class PaymentDetailsDao {

	@Autowired
	private SessionFactory sessionFactory;

	public PaymentDetails getPaymentDetails(Long reservationId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from PaymentDetails where reservationId=:reservationId");
		query.setParameter("reservationId", reservationId);

		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}

		return (PaymentDetails) query.list().get(0);
	}

	public void insertPaymentDetails(PaymentDetails paymentDetails) {
		sessionFactory.getCurrentSession().save(paymentDetails);
	}

}
