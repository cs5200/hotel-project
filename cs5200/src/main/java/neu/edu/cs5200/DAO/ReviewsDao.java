package neu.edu.cs5200.DAO;

import java.util.List;

import neu.edu.cs5200.entity.Reviews;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO for Reviews
 * 
 * @author priyanka
 * 
 */

@Repository
public class ReviewsDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Reviews getReviews(Long review_number) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Reviews where review_number=:review_number");
		query.setParameter("review_number", review_number);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}
		
		return (Reviews) query.list().get(0);
	}
	
	public void deleteReviews(Long review_number) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from Reviews where review_number=:review_number");
		
		query.setParameter("review_number", review_number);
		query.executeUpdate();
	}
	
	public void insertReviews(Reviews reviews) {
		sessionFactory.getCurrentSession().save(reviews);
	}

	public Boolean isReviewed(Long reservationId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Reviews where reservationId=:reservationId");
		query.setParameter("reservationId", reservationId);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return false;
		}
		return true;
	}

	public Long getCountByHotelId(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"select count(*) from Reviews where hotelId=:hotelId");
		query.setParameter("hotelId", hotelId);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return 0L;
		}
		return (Long) query.list().get(0);
	}


}
