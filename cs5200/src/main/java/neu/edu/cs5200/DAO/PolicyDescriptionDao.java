package neu.edu.cs5200.DAO;

import java.util.List;
import neu.edu.cs5200.entity.PolicyDescription;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO for PolicyDescription
 * 
 * @author vikas
 * 
 */
@Repository
public class PolicyDescriptionDao {
	@Autowired
	private SessionFactory sessionFactory;

	public PolicyDescription getPolicyDescription(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from PolicyDescription where hotelId=:hotelId");
		query.setParameter("hotelId", hotelId);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}
		
		return (PolicyDescription) query.list().get(0);
	}

	
	public void deletePolicyDescription(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from PolicyDescription where hotelId=:hotelId");
		
		query.setParameter("hotelId", hotelId);
		query.executeUpdate();
	}
	
	
	
	public void updatePolicyDescription(PolicyDescription policyDescription) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"update PolicyDescription set polDesc=:polDesc," +
				"languageCode=:languageCode where hotelId=:hotelId");
		
		query.setParameter("hotelId", policyDescription.getHotelId());
		query.setParameter("polDesc", policyDescription.getPolDesc());
		query.setParameter("languageCode", "en_US");
		
		query.executeUpdate();
		//sessionFactory.getCurrentSession().getTransaction().commit();
	}
	
	public void insertPolicyDescription(PolicyDescription policyDescription) {
			sessionFactory.getCurrentSession().save(policyDescription);
		//sessionFactory.getCurrentSession().getTransaction().commit();
	}
}
