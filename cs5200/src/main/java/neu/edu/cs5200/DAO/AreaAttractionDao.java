package neu.edu.cs5200.DAO;

import java.util.List;



import neu.edu.cs5200.entity.AreaAttraction;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO for AreaAttraction
 * 
 * @author vikas
 * 
 */
@Repository
public class AreaAttractionDao {

	@Autowired
	private SessionFactory sessionFactory;

	public AreaAttraction getAreaAttraction(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from AreaAttraction where hotelId=:hotelId");
		query.setParameter("hotelId", hotelId);
		
		List<?> results = query.list();
		if (results == null || results.size() == 0) {
			return null;
		}
		
		return (AreaAttraction) query.list().get(0);
	}
	
	
	public void deleteAreaAttraction(Long hotelId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"delete from AreaAttraction where hotelId=:hotelId");
		
		query.setParameter("hotelId", hotelId);
		query.executeUpdate();
	}

	
	public void updateAreaAttraction(AreaAttraction areaAttraction) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"update AreaAttraction set areaAttractions=:areaAttractions," +
				"languageCode=:languageCode where hotelId=:hotelId");
		
		query.setParameter("hotelId", areaAttraction.getHotelId());
		query.setParameter("areaAttractions", areaAttraction.getAreaAttractions());
		query.setParameter("languageCode", "en_US");
		
		query.executeUpdate();
		//sessionFactory.getCurrentSession().getTransaction().commit();
	}
	
	public void insertAreaAttraction(AreaAttraction areaAttraction) {
			sessionFactory.getCurrentSession().save(areaAttraction);
		//sessionFactory.getCurrentSession().getTransaction().commit();
	}
}
