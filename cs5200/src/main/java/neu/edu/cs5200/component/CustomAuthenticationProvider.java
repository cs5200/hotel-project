package neu.edu.cs5200.component;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
 
   // @Autowired
   // private UserService userService;
 
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();
 

         System.out.println("Username is "+username);
        return new UsernamePasswordAuthenticationToken(username, password);
    }
 
    @Override
    public boolean supports(Class<?> arg0) {
        return true;
    }
}