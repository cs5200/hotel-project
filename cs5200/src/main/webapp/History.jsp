<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html ng-app="app">
<head>
<title>Book-a-Hotel</title>

<!--  TODO: Move this links to a separate file and link them -->
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link rel="stylesheet" href="<c:url value="/resources/angucomplete-master/angucomplete.css"/>">

<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-sanitize.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-resource.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-0.11.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-transition.js" />"></script>

<!--  Initialize the app and bootstrap -->
<script type="text/javascript">
	var app = angular.module('app', ['ngResource','ui.bootstrap']).config(function($locationProvider, $resourceProvider) {
		/* For retrieving the query parameters */
		$locationProvider.html5Mode(true);
	});
	
</script>


<!--  TODO: Move this content to a separate file -->
<script type="text/javascript">

     var baseUrl = 'http://localhost:8080/book-a-hotel/';
     
     /**
      * Controller for Details.
      * Use http to connect the url using request parameters city and country,  retrieves information about it.
      * Bind it to the details object.
      *
      **/
	function MainController($scope, $http, $location,  $resource, $window) {
		var url = baseUrl+'get-history/';
		/* Hit the Url and get the response */
		$http.get(url).success(function(res) {
			
			$scope.reservationList = res;
			
			   /* Paginate the results */
			  $scope.totalItems = $scope.reservationList.length;
			  $scope.currentPage = 1;
			  $scope.numPerPage = 5;
			 
			  $scope.paginate = function(value) {
			    var begin, end, index;
			    begin = ($scope.currentPage - 1) * $scope.numPerPage;
			    end = begin + $scope.numPerPage;
			    index = $scope.reservationList.indexOf(value);
			    return (begin <= index && index < end);
			  };
		}).error(function(res){});
	}
     
	function rowController($scope, $http, $location,  $resource, $window) {
		var url = baseUrl+'admin/get-active-property/'+$scope.reservation.hotelId;
        var reviewUrl = baseUrl+'is-reviewed/'+$scope.reservation.reservationId;
        
		$scope.isCancelledDisabled = false;
		$scope.row = 'info';
		$scope.isReviewedDisabled = false;
		
		
		
		/* Hit the Url and get the response */
		$http.get(url).success(function(res) {
			$scope.hotelName= res.name+','+res.city;
		}).error(function(res){});
		
		var checkoutDate = new Date($scope.reservation.checkoutDate);
		var today = new Date();
		
		if(checkoutDate < today)
		{
			$scope.row = 'success';
			$scope.isCancelledDisabled = true;
		}
		else
		{
			$scope.isReviewedDisabled = true;
		}
		
		if($scope.reservation.isCancelled)
	    {
			$scope.row = 'danger';
			$scope.isCancelledDisabled = true;
			$scope.isReviewedDisabled = true;
		}
		
		/* Hit the Url and get the response */
		$http.get(reviewUrl).success(function(res) {
			if(res === 'false')
			{
				
				if(checkoutDate < today)
				{

				$scope.isReviewedDisabled = false;
				}
			}
			else
			{
				
				$scope.isReviewedDisabled = true;
			}
		}).error(function(res){});
		
		$scope.cancelReservation = function()
		{
			 $window.location.href = baseUrl+'Confirmation.jsp?reservationId='+$scope.reservation.reservationId;
		}
		
		$scope.summary = function()
		{
			 $window.location.href =baseUrl+'Confirmation.jsp?reservationId='+$scope.reservation.reservationId;
		}
		
		$scope.review = function()
		{
			 $window.location.href = baseUrl+'Review.jsp?reservationId='+$scope.reservation.reservationId+'&hotelName='+$scope.hotelName+'&hotelId='+$scope.reservation.hotelId;
		}
	}
     
</script>
</head>
<body >
<nav class="navbar navbar-inverse" style="background-color: #5CADFF;border: 0px;" role="navigation">
   <div class="container-fluid">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<%-- <ul class="nav navbar-nav">
		  <li class="active"><a href="<c:url value="/Welcome.jsp"/>" target="_self">Book a Hotel</a></li>
		</ul>
 --%>
				<span style="font-size: 28px; color: white;"><a
					href="/book-a-hotel/Welcome.jsp" target="_self"
					style="color: white;">Book a Hotel</a></span>


		<ul class="nav navbar-nav navbar-right">
		   <li class="active"><sec:authorize access="!isAuthenticated()">
                  <a href="<c:url value="/Login.jsp"/>">Login</a>
                </sec:authorize>
            </li>
    
            <li class="active">
			   <sec:authorize access="isAuthenticated()">
                 <a href="<c:url value="/History.jsp"/>" target="_self">Booking History</a>
                </sec:authorize>
		      </li>
					
               <li class="active"><sec:authorize access="isAuthenticated()">
                    <a href="<c:url value="/Logout.jsp"/>" target="_self">Logout</a>
                   </sec:authorize>
               </li>
		
			    <li class="active"><a href="<c:url value="/AboutUs.jsp"/>" target="_self">About Us</a></li>
		    </ul>
	    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
 </nav>
<div class="container-fluid">

    <h3> <span class="label label-primary">Booking History</span></h3>
<div class="row-fluid" ng-controller="MainController">
<table class="table">
 <tbody>
  <tr>
     <th>Reservation Id</th>
     <th>Hotel Name</th>
     <th>Checkin Date</th><th>Checkout Date</th><th>Cancelled</th>
     <th>Review</th>
     <th>Summary</th>
  </tr>
 <tr ng-repeat="reservation in reservationList | filter : paginate" ng-class="row" ng-controller="rowController">
 <td>RES{{reservation.reservationId}}</td>
   <td>{{hotelName}}</td>
  <td>{{reservation.checkinDate}}</td><td>{{reservation.checkoutDate}}</td> 
   <td><button type="button" class="btn btn-primary btn-sm" ng-disabled="isCancelledDisabled" ng-click="cancelReservation()">Cancel Reservation</button></td>
   <td><button type="button" class="btn btn-primary btn-sm" ng-disabled="isReviewedDisabled" ng-click="review()">Give Review</button></td>
   <td><a href ng-click="summary()">Summary</a></td>
 </tr>
 </tbody>
</table>

<div class="col-lg-4"></div>
 <div class="col-lg-8">
    <pagination total-items="totalItems" ng-model="currentPage"
          max-size="5" boundary-links="true"
          items-per-page="numPerPage" class="pagination-sm">
        </pagination>
  </div>
</div>
</div>
 <div class="navbar navbar-default navbar-fixed-bottom">

     <div class="container" style="text-align:center;font-size:12px;">
     
     	<p>Copyright &copy; 2014 </p>
     	<p>Developed by Vikas Joshi & Priyanka Narendran for CS-5200</p>
     <!-- <p class="muted pull-right"> Created by Z the Man of The master Plan</p>
     <p class="muted pull-left"> Created by Z the Man of The master Plan</p>
 -->
   </div> <!-- container-->
   </div> <!-- navbar navbar-default navbar-fixed-bottom" --> 
</body>
</html>