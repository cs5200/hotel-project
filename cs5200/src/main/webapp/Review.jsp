<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html ng-app="app">
<head>
<title>Book-a-Hotel</title>

<!--  TODO: Move this links to a separate file and link them -->
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link rel="stylesheet" href="<c:url value="/resources/angucomplete-master/angucomplete.css"/>">

<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-sanitize.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-resource.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-0.11.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-transition.js" />"></script>

<!--  Initialize the app and bootstrap -->
<script type="text/javascript">
	var app = angular.module('app', ['ngResource',  'ui.bootstrap']).config(function($locationProvider, $resourceProvider) {
		/* For retrieving the query parameters */
		$locationProvider.html5Mode(true);
	});
	
</script>


<!--  TODO: Move this content to a separate file -->
<script type="text/javascript">

     var baseUrl = 'http://localhost:8080/book-a-hotel/';
     
     /**
      * Controller for Details.
      * Use http to connect the url using request parameters city and country,  retrieves information about it.
      * Bind it to the details object.
      *
      **/
	function MainController($scope, $http, $location,  $resource, $window) {
    	  $scope.reviews={};
		$scope.reviews.reservationId = $location.search()['reservationId'];
		$scope.reviews.hotelId = $location.search()['hotelId'];
		$scope.hotelName = $location.search()['hotelName'];
		
		$scope.max = 5;
		
		$scope.saveReview = function()
		{
			// Delete the attractions
	  	    	 $http.post(baseUrl+'create-reviews/', $scope.reviews).success(function(res) {
	  		           
	  	    	  $scope.expect = '';
	  	    	  $scope.delete_expect_button = false;
		  	      $scope.create_expect = true;
	  	         }).error(function(res){$scope.delete_expect_button = false;});
		}
		
	}
     
</script>
</head>
<body >
<nav class="navbar navbar-inverse" style="background-color: #5CADFF;border: 0px;" role="navigation">
   <div class="container-fluid">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<%-- <ul class="nav navbar-nav">
		  <li class="active"><a href="<c:url value="/Welcome.jsp"/>" target="_self">Book a Hotel</a></li>
		</ul>
 --%>
				<span style="font-size: 28px; color: white;"><a
					href="/book-a-hotel/Welcome.jsp" target="_self"
					style="color: white;">Book a Hotel</a></span>


		<ul class="nav navbar-nav navbar-right">
		   <li class="active"><sec:authorize access="!isAuthenticated()">
                  <a href="<c:url value="/Login.jsp"/>">Login</a>
                </sec:authorize>
            </li>
    
            <li class="active">
			   <sec:authorize access="isAuthenticated()">
                 <a href="<c:url value="/History.jsp"/>" target="_self">Booking History</a>
                </sec:authorize>
		      </li>
					
               <li class="active"><sec:authorize access="isAuthenticated()">
                    <a href="<c:url value="/Logout.jsp"/>" target="_self">Logout</a>
                   </sec:authorize>
               </li>
		
			    <li class="active"><a href="<c:url value="/AboutUs.jsp"/>" target="_self">About Us</a></li>
		    </ul>
	    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
 </nav>
 <div class="container-fluid">
   <h3 style="padding: 18px;"> Review </h3>
<div class="row" ng-controller="MainController">
 <form class="form-horizontal" >
    <div class="form-group">
        <label class="col-xs-2 control-label">Reservation Id</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="reviews.reservationId"   readonly="true" class="form-control">
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-xs-2 control-label">Hotel Name</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="hotelName" readonly="true" required="required" class="form-control">
        </div>
    </div>

     <div class="form-group">
        <label class="col-xs-2 control-label">Review</label>
        <div class="col-lg-5">
            <textarea rows="8" cols="100" ng-model="reviews.review" name="reviews.review"  class="form-control">
           </textarea>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-xs-2 control-label">Rating</label>
        <div class="col-lg-5">
            <rating ng-model="reviews.rating" max="max"  on-hover="hoveringOver(value)" on-leave="overStar = null"></rating>
        </div>
    </div>
  				  
    <div class="form-group">
        <div class="col-xs-offset-3 col-xs-9">
           <input type="submit" class="btn btn-primary" value="Submit"  data-toggle="modal" data-target="#saveModal" ng-click="saveReview()">
        </div>
    </div>
    
  <div id="saveModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="alert alert-success alert-dismissible" role="alert">Your review has been recorded Succesfully</div>
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer"><button type="button" data-dismiss="modal" class="btn btn-primary">OK</button></div>
    </div>
  </div>
  </div>
  
  <div id="deleteModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="alert alert-success alert-dismissible" role="alert">Delete was Succesful</div>
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer"><button type="button" data-dismiss="modal" class="btn btn-primary">OK</button></div>
    </div>
  </div>
  </div>
  
  </form>
</div>
</div>

 <div class="navbar navbar-default navbar-fixed-bottom">

     <div class="container" style="text-align:center;font-size:12px;">
     
     	<p>Copyright &copy; 2014 </p>
     	<p>Developed by Vikas Joshi & Priyanka Narendran for CS-5200</p>
     <!-- <p class="muted pull-right"> Created by Z the Man of The master Plan</p>
     <p class="muted pull-left"> Created by Z the Man of The master Plan</p>
 -->
   </div> <!-- container-->
   </div> <!-- navbar navbar-default navbar-fixed-bottom" --> 
</body>
</html>