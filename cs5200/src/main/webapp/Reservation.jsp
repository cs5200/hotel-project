<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html ng-app="app">
<head>
<title>Book-a-Hotel</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  TODO: Move this links to a separate file and link them -->
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link rel="stylesheet" href="<c:url value="/resources/angucomplete-master/angucomplete.css"/>">

<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-sanitize.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-resource.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-0.11.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-transition.js" />"></script>

<!--  Initialize the app and bootstrap -->
<script type="text/javascript">
	var app = angular.module('app', ['ngResource']).config(function($locationProvider, $resourceProvider) {
		/* For retrieving the query parameters */
		$locationProvider.html5Mode(true);
	});
	
</script>


<!--  TODO: Move this content to a separate file -->
<script type="text/javascript">

     var baseUrl = 'http://localhost:8080/book-a-hotel/';
     
     /**
      * Controller for Details.
      * Use http to connect the url using request parameters city and country,  retrieves information about it.
      * Bind it to the details object.
      *
      **/
	function MainController($scope, $http, $location,  $resource, $window) {
    		$scope.payment = false;
    		$scope.reservation={};
    		$scope.paymentDetails={};
		    $scope.hotelId = $location.search()['hotelId'];
		    $scope.isCancelled = false;
		    $scope.customerId =  $location.search()['customerId'];
		    			
		    $scope.checkoutDate = $location.search()['checkout'];
		    $scope.price = $location.search()['price'];
		    $scope.hotelName = $location.search()['hotelName'];
		    $scope.rooms = $location.search()['rooms'];
		    $scope.checkinDate = $location.search()['checkin']; 
		    $scope.adults = $location.search()['adults'];  
		    $scope.children = $location.search()['children'];  
		    $scope.bedType = $location.search()['bedType'];  
		    
		    
		    $http.get(baseUrl+'get-customer/').success(function(res) {
				  
		    	$scope.reservation.firstName = res.firstName;
		    	$scope.reservation.lastName = res.lastName;
		    	$scope.reservation.emailId = res.emailId;
		    	$scope.reservation.phoneNumber = res.phoneNum;
		    	$scope.paymentDetails.emailAddress = res.emailId;
		    	$scope.paymentDetails.billingAddress1 = res.address1;
		    	$scope.paymentDetails.billingAddress2 = res.address2;
		    	$scope.paymentDetails.city = res.city;
 			}).error(function(res){$scope.delete_cancel_button = false;});
		    
		    
		    
		    $scope.reserve = function() {
		    	
		    	$scope.reservation.hotelId = $scope.hotelId;
				$scope.reservation.isCancelled = $scope.isCancelled;
				$scope.reservation.customerId = $scope.customerId;
				$scope.reservation.checkoutDate = $scope.checkoutDate;
				$scope.reservation.price = $scope.price;
				$scope.reservation.rooms = $scope.rooms;
				$scope.reservation.checkinDate = $scope.checkinDate;
				$scope.reservation.adults = $scope.adults;
				$scope.reservation.children = $scope.children;
				$scope.reservation.bedType = $scope.bedType;
				
				$scope.reservationId = '';	
				var dataObject = {
					customerId: $scope.reservation.customerId,
					hotelId: $scope.reservation.hotelId,
					checkinDate: $scope.reservation.checkinDate,
					rooms: $scope.reservation.rooms,
					price: $scope.reservation.price,
					isCancelled: $scope.reservation.isCancelled,
					firstName: $scope.reservation.firstName,
					lastName:  $scope.reservation.lastName,
					emailId:  $scope.reservation.emailId,
					phoneNumber:  $scope.reservation.phoneNumber,
					checkoutDate: $scope.reservation.checkoutDate,
					adults: $scope.reservation.adults,
					children: $scope.reservation.children,
					bedType: $scope.reservation.bedType 
				};
		    	 $http.post(baseUrl+'create-reservation/', dataObject,{}).success(function(res) 
			    		    {
		    		     
			    		  	$scope.reservation = res;
			    		  	$scope.reservationId  = res.reservationId;
			    		  	$scope.payment = true;
			    		    }).error(function(res){	$scope.payment = false;});
		    	
		    }
		    
		    $scope.completeBooking = function()
		    {
		    	$scope.paymentDetails.reservationId = $scope.reservationId;
		    	var paymentDetailsObject = {
		    		reservationId: $scope.reservationId,
		    		cardholderName: $scope.paymentDetails.cardholderName,
		    		cardNumber: $scope.paymentDetails.cardNumber,
		    		expMonth: $scope.paymentDetails.expMonth,
		    		expYear: $scope.paymentDetails.expYear,
		    		securityCode: $scope.paymentDetails.securityCode,
		    		country: $scope.paymentDetails.country,
		    		emailAddress: $scope.paymentDetails.emailAddress,
		    		billingAddress1: $scope.paymentDetails.billingAddress1,
		    		billingAddress2: $scope.paymentDetails.billingAddress2,
		    		city: $scope.paymentDetails.city,
		    		postalCode: $scope.paymentDetails.postalCode
		    		
		    	};
		    	 $http.post(baseUrl+'create-payment-details/', paymentDetailsObject, {}).success(function(res) 
			     {
		    		 $window.location.href =baseUrl+'Confirmation.jsp?reservationId='+$scope.reservationId;
			     }).error(function(res){	/* TODO: ROLL BACK Reservation and payment*/});
		    }
		
	}
     
</script>
</head>
<body >
<nav class="navbar navbar-inverse" style="background-color: #5CADFF;border: 0px;" role="navigation">
   <div class="container-fluid">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<%-- <ul class="nav navbar-nav">
		  <li class="active"><a href="<c:url value="/Welcome.jsp"/>" target="_self">Book a Hotel</a></li>
		</ul>
 --%>
				<span style="font-size: 28px; color: white;"><a
					href="/book-a-hotel/Welcome.jsp" target="_self"
					style="color: white;">Book a Hotel</a></span>


		<ul class="nav navbar-nav navbar-right">
		   <li class="active"><sec:authorize access="!isAuthenticated()">
                  <a href="<c:url value="/Login.jsp"/>">Login</a>
                </sec:authorize>
            </li>
    
            <li class="active">
			   <sec:authorize access="isAuthenticated()">
                 <a href="<c:url value="/History.jsp"/>" target="_self">Booking History</a>
                </sec:authorize>
		      </li>
					
               <li class="active"><sec:authorize access="isAuthenticated()">
                    <a href="<c:url value="/Logout.jsp"/>" target="_self">Logout</a>
                   </sec:authorize>
               </li>
		
			    <li class="active"><a href="<c:url value="/AboutUs.jsp"/>" target="_self">About Us</a></li>
		    </ul>
	    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
 </nav>
<div class="container-fluid" ng-controller="MainController">
  <div class="row">
     <div class="col-md-8">
     <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Guest Information</h3>
        </div>
       <div class="panel-body">
       <form class="form-horizontal" >
        <div class="form-group">
          <label class="col-xs-2 control-label">First Name</label>
          <div class="col-lg-5">
           <input type="text" id="1" ng-model="reservation.firstName"  required="required" class="form-control">
          </div>
         </div>
    
       <div class="form-group">
         <label class="col-xs-2 control-label">Last Name</label>
         <div class="col-lg-5">
           <input type="text" id="2" ng-model="reservation.lastName" required="required" class="form-control">
        </div>
       </div>

       <div class="form-group">
         <label class="col-xs-2 control-label">Email Id</label>
         <div class="col-lg-5">
           <input type="text" id="3" ng-model="reservation.emailId" required="required" class="form-control">
         </div>
       </div>
    
       <div class="form-group">
        <label class="col-xs-2 control-label">Phone Number</label>
         <div class="col-lg-5">
           <input type="text" id="4" ng-model="reservation.phoneNumber" required="required" class="form-control">
         </div>
       </div>
       
       <div class="form-group">
        <div class="col-xs-offset-10 col-xs-12">
            <input type="submit" class="btn btn-primary btn-lg " value="Next" ng-click="reserve()">
        </div>
    </div>
     </form>
     </div>
     </div>
     
     
     <div class="panel panel-primary" ng-show="payment">
        <div class="panel-heading">
          <h3 class="panel-title">Payment Details</h3>
        </div>
       <div class="panel-body">
       <form class="form-horizontal" >
        <div class="form-group">
          <label class="col-xs-2 control-label">Cardholder Name</label>
          <div class="col-lg-5">
           <input type="text" id="5" ng-model="paymentDetails.cardholderName" class="form-control">
          </div>
         </div>
    
       <div class="form-group">
         <label class="col-xs-2 control-label">Card Number</label>
         <div class="col-lg-5">
           <input type="text" id="6" ng-model="paymentDetails.cardNumber" required="required" class="form-control">
        </div>
       </div>

       <div class="form-group">
         <label class="col-xs-2 control-label">Expiration Month</label>
         <div class="col-lg-5">
           <input type="text" id="7" ng-model="paymentDetails.expMonth" required="required" class="form-control">
         </div>
       </div>
    
       <div class="form-group">
        <label class="col-xs-2 control-label">Expiration Year</label>
         <div class="col-lg-5">
           <input type="text" id="8" ng-model="paymentDetails.expYear" required="required" class="form-control">
         </div>
       </div>
       
       <div class="form-group">
        <label class="col-xs-2 control-label">Security Code</label>
         <div class="col-lg-5">
           <input type="text" id="9" ng-model="paymentDetails.securityCode" required="required" class="form-control">
         </div>
       </div>
       
       
       <div class="form-group">
        <label class="col-xs-2 control-label">Country</label>
         <div class="col-lg-5">
           <input type="text" id="10" ng-model="paymentDetails.country" required="required" class="form-control">
         </div>
       </div>
       
       
       <div class="form-group">
        <label class="col-xs-2 control-label">Billing Email Id</label>
         <div class="col-lg-5">
           <input type="text" id="11" ng-model="paymentDetails.emailAddress" required="required" class="form-control">
         </div>
       </div>
       
       
       <div class="form-group">
        <label class="col-xs-2 control-label">Billing Address1</label>
         <div class="col-lg-5">
           <input type="text" id="12" ng-model="paymentDetails.billingAddress1" required="required" class="form-control">
         </div>
       </div>
       
       
       <div class="form-group">
        <label class="col-xs-2 control-label">Billing Address2</label>
         <div class="col-lg-5">
           <input type="text" id="13" ng-model="paymentDetails.billingAddress2" required="required" class="form-control">
         </div>
       </div>
       
       
       <div class="form-group">
        <label class="col-xs-2 control-label">City</label>
         <div class="col-lg-5">
           <input type="text" id="14" ng-model="paymentDetails.city" required="required" class="form-control">
         </div>
       </div>
       
       
       <div class="form-group">
        <label class="col-xs-2 control-label">Postal Code</label>
         <div class="col-lg-5">
           <input type="text" id="16" ng-model="paymentDetails.postalCode" required="required" class="form-control">
         </div>
       </div>
       
       <div class="form-group">
        <div class="col-xs-offset-9 col-xs-12">
            <input type="submit" class="btn btn-primary btn-lg " value="Complete Booking" ng-click="completeBooking()">
        </div>
    </div>
     </form>
     </div>
     </div>
     
     
   </div> <!--  End of sidebar columns -->
   
   <div class="col-md-4">
  <div class="panel panel-primary">
      <div class="panel-heading"> Summary</div>
      <div class="panel-body">
      <label class="control-label" style="min-width: 128px;">Hotel:</label>{{ hotelName }}<br/>
      <label class="control-label" style="min-width: 128px;">Checkin Date:</label>{{ checkinDate }}<br/>
      <label class="control-label" style="min-width: 128px;">Checkout Date:</label>{{ checkoutDate }}<br/>
      <label class="control-label" style="min-width: 128px;">Number of rooms:</label>{{ rooms }}<br/>
      <label class="control-label" style="min-width: 128px;">Adults:</label>{{adults}}<br/>
      <label class="control-label" style="min-width: 128px;">Children:</label>{{children}}<br/>
      <label class="control-label" style="min-width: 128px;">Bed Type:</label>{{bedType}}<br/>
      <label class="control-label" style="min-width: 128px;">Total Price:</label>$ {{ price }}
      </div>
  </div>
    </div>
     
   <!-- <div class="col-md-4">
      <h3> Summary</h3>
      <p><b>
      Hotel: {{ hotelName }}<br/>
      Checkin Date: {{ checkinDate }}<br/>
      checkout Date: {{ checkoutDate }}<br/>
      Number of rooms: {{ rooms }}<br/>
      Adults: {{adults}}<br/>
      Children: {{children}}<br/>
      Bed Type: {{bedType}}<br/>
      Total Price:$ {{ price }}</b>
      </p>
    </div> -->
   </div><!-- row -->
  </div><!-- container -->
 <div class="navbar navbar-default navbar-fixed-bottom">

     <div class="container" style="text-align:center;font-size:12px;">
     
     	<p>Copyright &copy; 2014 </p>
     	<p>Developed by Vikas Joshi & Priyanka Narendran for CS-5200</p>
     <!-- <p class="muted pull-right"> Created by Z the Man of The master Plan</p>
     <p class="muted pull-left"> Created by Z the Man of The master Plan</p>
 -->
   </div> <!-- container-->
   </div> <!-- navbar navbar-default navbar-fixed-bottom" --> 
</body>

</html>