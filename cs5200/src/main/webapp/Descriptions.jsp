<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html ng-app="app">
<head>
<title>Book-a-Hotel</title>

<!--  TODO: Move this links to a separate file and link them -->
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link rel="stylesheet" href="<c:url value="/resources/angucomplete-master/angucomplete.css"/>">

<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-sanitize.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-resource.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-0.11.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-transition.js" />"></script>

<!--  Initialize the app and bootstrap -->
<script type="text/javascript">
	var app = angular.module('app', ['ngResource']).config(function($locationProvider, $resourceProvider) {
		/* For retrieving the query parameters */
		$locationProvider.html5Mode(true);
	});
	
</script>


<!--  TODO: Move this content to a separate file -->
<script type="text/javascript">

     var baseUrl = 'http://localhost:8080/book-a-hotel/';
     
     /**
      * Controller for Details.
      * Use http to connect the url using request parameters city and country,  retrieves information about it.
      * Bind it to the details object.
      *
      **/
	function MainController($scope, $http, $location,  $resource) {
		$scope.hotelId = $location.search()['hotelId'];
	
		$scope.attractions='';
		$scope.create_attr=false;
		$scope.delete_attractions_button = true;
		
		
		$scope.recreations='';
		$scope.create_rec=false;
		$scope.delete_rec_button = true;
		
		$scope.policies='';
		$scope.create_pol=false;
		$scope.delete_pol_button = true;
		
		
		$scope.spa='';
		$scope.create_spa=false;
		$scope.delete_spa_button = true;
		
		$scope.expect='';
		$scope.create_expect=false;
		$scope.delete_expect_button = true;
		
		
		$scope.dining='';
		$scope.create_dining=false;
		$scope.delete_dining_button = true;
		
		
		if (typeof $scope.hotelId  != 'undefined')
	    {

		   var areaUrl = baseUrl+'area-attraction?hotelId='+ $scope.hotelId;
		   var policyUrl = baseUrl+'policy-description?hotelId='+ $scope.hotelId;
		   var recreationUrl =baseUrl+'recreation-description?hotelId='+ $scope.hotelId;
		   var spaUrl = baseUrl+'spa-description?hotelId='+ $scope.hotelId;
		   var expectUrl = baseUrl+'what-to-expect-description?hotelId='+ $scope.hotelId;
		   var diningUrl = baseUrl+'dining-description?hotelId='+ $scope.hotelId;
		

		    /* Get the area attractions and bind it to the object */
			$http.get(areaUrl).success(function(res) {
				 $scope.attractions = res;
				 $scope.attractions.areaAttractions = res.areaAttractions;
				 if($scope.attractions == null || $scope.attractions == '')
				 {
				     $scope.create_attr = true;	 
				     $scope.delete_attractions_button = false;
			     }
			});
						
		    /* Get policy Description and bind it to the object */
			$http.get(policyUrl).success(function(res) {
				$scope.policies = res;
				$scope.policies.polDesc = res.polDesc;
				 if($scope.policies == null || $scope.policies == '')
				 {
				     $scope.create_pol = true;	 
				     $scope.delete_pol_button = false;
			     }
			    
			});
			
		    /* Get the recreation descriptions */
			$http.get(recreationUrl).success(function(res) {
				$scope.recreations = res;
				$scope.recreations.recreationDesc = res.recreationDesc;
				if($scope.recreations == null || $scope.recreations == '')
				 {
				     $scope.create_rec = true;	 
				     $scope.delete_rec_button = false;
			     }
			   
			});
		    
		    
		    /* Get the spa descriptions */
			$http.get(spaUrl).success(function(res) {
				$scope.spa = res;
				$scope.spa.spaDesc = res.spaDesc;
				if($scope.spa == null || $scope.spa == '')
				 {
				     $scope.create_spa = true;	 
				     $scope.delete_spa_button = false;
			     }
			   
			});
		    
		    
			/* Get the whattoexpect descriptions */
			$http.get(expectUrl).success(function(res) {
				$scope.expect = res;
				$scope.expect.whatToExpect = res.whatToExpect;
				if($scope.expect == null || $scope.expect == '')
				 {
				     $scope.create_expect = true;	 
				     $scope.delete_expect_button = false;
			     }
			   
			});
			
			
			/* Get the dining descriptions */
			$http.get(diningUrl).success(function(res) {
				$scope.dining = res;
				$scope.dining.diningDesc = res.diningDesc;
				if($scope.dining == null || $scope.dining == '')
				 {
				     $scope.create_dining = true;	 
				     $scope.delete_dining_button = false;
			     }
			   
			});
	  }//End of if
	
    /****************************************** Attractions ****************************************/
     $scope.saveAttractions = function() {
		  
	    	
	    	// Check for not null of hotelId, if it is not specified create a new object
	    if($scope.create_attr == true)
	    {
	    		    $scope.attractions.hotelId = $scope.hotelId;
	    		    $scope.delete_attractions_button = false;
	    		    // Create a new active property on save
	    		    $http.post(baseUrl+'admin/create-area-attraction/', $scope.attractions).success(function(res) 
	    		    {
	    		  	  $scope.attractions = res;
	    		  	  $scope.delete_attractions_button = true;
	    		  	  $scope.create_attr = false;
	    		    }).error(function(res){ $scope.delete_attractions_button = false;});
	    		}
	    	else
	    		{
	    		   // If hotelId exists, update the active property at the backend
	    	       $http.post(baseUrl+'admin/update-area-attraction/'+$scope.hotelId, $scope.attractions).success(function(res) {
		           $scope.attractions = res;
	    	     }).error(function(res){ $scope.delete_attractions_button = false;});
			}
	    } //End of save attractions
	    
	    
	   $scope.deleteAttractions = function() {
	    if($scope.create_attr == true)
	    {
	    	$scope.delete_attractions_button = false;
	    }
	    else
	    {
	        // Delete the attractions
	    	$http.post(baseUrl+'admin/delete-area-attraction/'+$scope.hotelId, $scope.attractions).success(function(res) {
		           
	    	$scope.attractions = '';
	    	$scope.delete_attractions_button = false;
	    	$scope.create_attr = true
	    	}).error(function(res){$scope.delete_attractions_button = false;});
	     }
	    }//End of delete property
	    
	  /****************************************** EndAttractions ****************************************/
	  
	  
	  /****************************************** Recreation ****************************************/
	       $scope.saveRecreation = function() {
	  	    	
	  	    	// Check for not null of hotelId, if it is not specified create a new object
	  	    	if($scope.create_rec == true)
	  	    		{
	  	    		    $scope.recreations.hotelId = $scope.hotelId;
	  	    		    $scope.delete_rec_button = false;
	  	    		    // Create a new active property on save
	  	    		    $http.post(baseUrl+'admin/create-rec-desc/', $scope.recreations).success(function(res) 
	  	    		    {
	  	    		  	  $scope.recreations = res;
	  	    		  	  $scope.delete_rec_button = true;
	  	    		  	  $scope.create_rec = false;
	  	    		    }).error(function(res){ $scope.delete_rec_button = false;});
	  	    		}
	  	    	else
	  	    		{
	  	    		   // If hotelId exists, update the active property at the backend
	  	    	       $http.post(baseUrl+'admin/update-rec-desc/'+$scope.hotelId, $scope.recreations).success(function(res) {
	  		           $scope.recreations = res;
	  		           $scope.delete_rec_button = true;
	  	    	     }).error(function(res){ $scope.delete_rec_button = false;});
	  			}
	  	    } //End of save recreations
	  	    
	  	    
	  	       $scope.deleteRecreation = function() {
	  	    	if($scope.create_rec == true)
	  	    		{
	  	    			$scope.delete_rec_button = false;
	  	    		}
	  	    	else
	  	    		{
	  	    		   // Delete the attractions
	  	    	       $http.post(baseUrl+'admin/delete-rec-desc/'+$scope.hotelId, $scope.recreations).success(function(res) {
	  		           
	  	    	       $scope.recreations = '';
	  	    	       $scope.delete_rec_button = false;
	  	    	       $scope.create_rec = true
	  	    	     }).error(function(res){$scope.delete_rec_button = false;});
	  			}
	  	    }//End of delete recreation
	  	    
	 /****************************************** End Recreation ****************************************/
	  
	 
	  	     
	 /****************************************** Policy Description ****************************************/
	    $scope.savePolicyDescription= function() {
	  		  	    	
	  		// Check for not null of hotelId, if it is not specified create a new object
	  		if($scope.create_pol == true)
	  		 {
	  		  	  $scope.policies.hotelId = $scope.hotelId;
	  		  	  $scope.delete_pol_button = false;
	  		  	  // Create a new active property on save
	  		  	  $http.post(baseUrl+'admin/create-pol-desc/', $scope.policies).success(function(res) 
	  		  	  {
	  		  	      $scope.policies = res;
	  		  	      $scope.delete_pol_button = true;
	  		  	      $scope.create_pol = false;
	  		  	   }).error(function(res){ $scope.delete_pol_button = false;});
	  		  }
	  		  else
	  		  {
	  		  	   // If hotelId exists, update the active property at the backend
	  		  	   $http.post(baseUrl+'admin/update-pol-desc/'+$scope.hotelId, $scope.policies).success(function(res) {
	  		  		       $scope.policies = res;
	  		  		       $scope.delete_pol_button = true;
	  		  	     }).error(function(res){ $scope.delete_pol_button = false;});
	  		   }
	  		} //End of save recreations
	  		  	    
	  		  	    
	  		 $scope.deletePolicyDescription = function() {
	  		  	    if($scope.create_pol == true)
	  		  	    {
	  		  	    	$scope.delete_pol_button = false;
	  		  	    }
	  		  	    else
	  		  	    {
	  		  	         // Delete the attractions
	  		  	    	 $http.post(baseUrl+'admin/delete-pol-desc/'+$scope.hotelId, $scope.policies).success(function(res) {
	  		  		           
	  		  	    	  $scope.policies = '';
	  		  	    	  $scope.delete_pol_button = false;
		  		  	      $scope.create_pol = true;
	  		  	         }).error(function(res){$scope.delete_pol_button = false;});
	  		  	    }
	  		  }//End of delete recreation
	  		  	    
	 /****************************************** End Policy Description ****************************************/
	
	  /****************************************** Spa Description ****************************************/
	 	    $scope.saveSpaDescription= function() {
	 	  		  	    	
	 	  		// Check for not null of hotelId, if it is not specified create a new object
	 	  		if($scope.create_spa == true)
	 	  		 {
	 	  		  	  $scope.spa.hotelId = $scope.hotelId;
	 	  		  	  $scope.delete_spa_button = false;
	 	  		  	  // Create a new spa description on save
	 	  		  	  $http.post(baseUrl+'admin/create-spa-description/', $scope.spa).success(function(res) 
	 	  		  	  {
	 	  		  	      $scope.spa = res;
	 	  		  	      $scope.delete_spa_button = true;
	 	  		  	      $scope.create_spa = false;
	 	  		  	   }).error(function(res){ $scope.delete_spa_button = false;});
	 	  		  }
	 	  		  else
	 	  		  {
	 	  		  	   // If hotelId exists, update the spa description at the backend
	 	  		  	   $http.post(baseUrl+'admin/update-spa-description/'+$scope.hotelId, $scope.spa).success(function(res) {
	 	  		  		       $scope.spa = res;
	 	  		  		  $scope.delete_spa_button = true;
	 	  		  	     }).error(function(res){ $scope.delete_spa_button = false;});
	 	  		   }
	 	  		} //End of save spa
	 	  		  	    
	 	  		  	    
	 	  		 $scope.deleteSpaDescription = function() {
	 	  		  	    if($scope.create_spa == true)
	 	  		  	    {
	 	  		  	    	$scope.delete_spa_button = false;
	 	  		  	    }
	 	  		  	    else
	 	  		  	    {
	 	  		  	         // Delete the spa
	 	  		  	    	 $http.post(baseUrl+'admin/delete-spa-description/'+$scope.hotelId, $scope.spa).success(function(res) {
	 	  		  		           
	 	  		  	    	  $scope.spa = '';
	 	  		  	    	  $scope.delete_spa_button = false;
	 		  		  	      $scope.create_spa = true;
	 	  		  	         }).error(function(res){$scope.delete_spa_button = false;});
	 	  		  	    }
	 	  		  }//End of spa description
	 	  		  	    
	 	 /****************************************** End spa Description ****************************************/
	 	  
	 	  /****************************************** Dining Description ****************************************/
	 		    $scope.saveDiningDescription= function() {
	 		  		  	    	
	 		  		// Check for not null of hotelId, if it is not specified create a new object
	 		  		if($scope.create_dining == true)
	 		  		 {
	 		  		  	  $scope.dining.hotelId = $scope.hotelId;
	 		  		  	  $scope.delete_dining_button = false;
	 		  		  	  // Create a new active property on save
	 		  		  	  $http.post(baseUrl+'admin/create-dining-description/', $scope.dining).success(function(res) 
	 		  		  	  {
	 		  		  	      $scope.dining = res;
	 		  		  	      $scope.delete_dining_button = true;
	 		  		  	      $scope.create_dining = false;
	 		  		  	   }).error(function(res){ $scope.delete_dining_button = false;});
	 		  		  }
	 		  		  else
	 		  		  {
	 		  		  	   // If hotelId exists, update the dining property at the backend
	 		  		  	   $http.post(baseUrl+'admin/update-dining-description/'+$scope.hotelId, $scope.dining).success(function(res) {
	 		  		  		       $scope.dining = res;
	 		  		  		  $scope.create_dining = true;
	 		  		  	     }).error(function(res){ $scope.delete_dining_button = false;});
	 		  		   }
	 		  		} //End of save recreations
	 		  		  	    
	 		  		  	    
	 		  		 $scope.deleteDiningDescription = function() {
	 		  		  	    if($scope.create_dining == true)
	 		  		  	    {
	 		  		  	    	$scope.delete_dining_button = false;
	 		  		  	    }
	 		  		  	    else
	 		  		  	    {
	 		  		  	         // Delete the dining
	 		  		  	    	 $http.post(baseUrl+'admin/delete-dining-description/'+$scope.hotelId, $scope.dining).success(function(res) {
	 		  		  		           
	 		  		  	    	  $scope.dining = '';
	 		  		  	    	  $scope.delete_dining_button = false;
	 			  		  	      $scope.create_dining = true;
	 		  		  	         }).error(function(res){$scope.delete_dining_button = false;});
	 		  		  	    }
	 		  		  }//End of delete dining
	 		  		  	    
	 		 /****************************************** End dining Description ****************************************/
	 		  
	 		/****************************************** Expect Description ****************************************/
	 			    $scope.saveExpectDescription= function() {
	 			  		  	    	
	 			  		// Check for not null of hotelId, if it is not specified create a new object
	 			  		if($scope.create_expect == true)
	 			  		 {
	 			  		  	  $scope.expect.hotelId = $scope.hotelId;
	 			  		  	  $scope.delete_expect_button = false;
	 			  		  	  // Create a new  expect on save
	 			  		  	  $http.post(baseUrl+'admin/create-what-to-expect-description/', $scope.expect).success(function(res) 
	 			  		  	  {
	 			  		  	      $scope.expect = res;
	 			  		  	      $scope.delete_expect_button = true;
	 			  		  	      $scope.create_expect = false;
	 			  		  	   }).error(function(res){ $scope.delete_expect_button = false;});
	 			  		  }
	 			  		  else
	 			  		  {
	 			  		  	   // If hotelId exists, update the expect description at the backend
	 			  		  	   $http.post(baseUrl+'admin/update-what-to-expect-description/'+$scope.hotelId, $scope.expect).success(function(res) {
	 			  		  		       $scope.expect = res;
	 			  		  		  $scope.delete_expect_button = true;
	 			  		  	     }).error(function(res){ $scope.delete_expect_button = false;});
	 			  		   }
	 			  		} //End of save except
	 			  		  	    
	 			  		  	    
	 			  		 $scope.deleteExpectDescription = function() {
	 			  		  	    if($scope.create_expect == true)
	 			  		  	    {
	 			  		  	    	$scope.delete_expect_button = false;
	 			  		  	    }
	 			  		  	    else
	 			  		  	    {
	 			  		  	         // Delete the attractions
	 			  		  	    	 $http.post(baseUrl+'admin/delete-what-to-expect-description/'+$scope.hotelId, $scope.expect).success(function(res) {
	 			  		  		           
	 			  		  	    	  $scope.expect = '';
	 			  		  	    	  $scope.delete_expect_button = false;
	 				  		  	      $scope.create_expect = true;
	 			  		  	         }).error(function(res){$scope.delete_expect_button = false;});
	 			  		  	    }
	 			  		  }//End of delete expect
	 			  		  	    
	  /****************************************** End expect Description ****************************************/
	 			  
	}
     
</script>
</head>
<body >
<nav class="navbar navbar-inverse" style="background-color: #5CADFF;border: 0px;" role="navigation">
   <div class="container-fluid">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<%-- <ul class="nav navbar-nav">
		  <li class="active"><a href="<c:url value="/Welcome.jsp"/>" target="_self">Book a Hotel</a></li>
		</ul>
 --%>
				<span style="font-size: 28px; color: white;"><a
					href="/book-a-hotel/Welcome.jsp" target="_self"
					style="color: white;">Book a Hotel</a></span>



		<ul class="nav navbar-nav navbar-right">
		   <li class="active"><sec:authorize access="!isAuthenticated()">
                  <a href="<c:url value="/Login.jsp"/>">Login</a>
                </sec:authorize>
            </li>
    
            <li class="active">
			   <sec:authorize access="isAuthenticated()">
                 <a href="<c:url value="/History.jsp"/>" target="_self">Booking History</a>
                </sec:authorize>
		      </li>
					
               <li class="active"><sec:authorize access="isAuthenticated()">
                    <a href="<c:url value="/Logout.jsp"/>" target="_self">Logout</a>
                   </sec:authorize>
               </li>
		
			    <li class="active"><a href="<c:url value="/AboutUs.jsp"/>" target="_self">About Us</a></li>
		    </ul>
	    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
 </nav>
<div class="container-fluid" ng-controller="MainController">
   <div class="panel panel-primary">
   <div class="panel-heading">
      <h3 class="panel-title">
         Area Attraction
      </h3>
   </div>
   <div class="panel-body">
     <textarea rows="8" cols="100" ng-model="attractions.areaAttractions" name="attractions.areaAttractions">
     </textarea>
   </div>
   <div class="panel-footer"> 
   <input type="submit" class="btn btn-primary" data-toggle="modal" data-target="#saveModal" value="Save" ng-click="saveAttractions()">
    <input type="submit" ng-show="delete_attractions_button" class="btn btn-primary"  data-toggle="modal" data-target="#deleteModal" value="Delete" ng-click="deleteAttractions()"></div>
  </div>

   <div class="panel panel-primary">
   <div class="panel-heading">
      <h3 class="panel-title">
         Recreation Description
      </h3>
   </div>
   <div class="panel-body">
       <textarea rows="8" cols="100" ng-model="recreations.recreationDesc" name="recreations.recreationDesc">
     </textarea>
   </div>
   <div class="panel-footer"> 
    <input type="submit" class="btn btn-primary" data-toggle="modal" data-target="#saveModal" value="Save" ng-click="saveRecreation()">
    <input type="submit" ng-show="delete_rec_button" class="btn btn-primary" data-toggle="modal" data-target="#deleteModal" value="Delete" ng-click="deleteRecreation()"></div>
   </div>

   <div class="panel panel-primary">
   <div class="panel-heading">
      <h3 class="panel-title">
         Policy Description
      </h3>
   </div>
   <div class="panel-body">
      <textarea rows="8" cols="100" ng-model="policies.polDesc" name="policies.polDesc">
     </textarea>
   </div>
   <div class="panel-footer"> 
    <input type="submit" class="btn btn-primary" data-toggle="modal" data-target="#saveModal" value="Save" ng-click="savePolicyDescription()">
    <input type="submit" ng-show="delete_pol_button" class="btn btn-primary"  data-toggle="modal" data-target="#deleteModal" value="Delete" ng-click="deletePolicyDescription()"></div>
</div>

   <div class="panel panel-primary">
   <div class="panel-heading">
      <h3 class="panel-title">
         Spa Description
      </h3>
   </div>
   <div class="panel-body">
     <textarea rows="8" cols="100" ng-model="spa.spaDesc" name="spa.spaDesc">
     </textarea>
   </div>
   <div class="panel-footer"> <input type="submit" class="btn btn-primary" data-toggle="modal" data-target="#saveModal"  value="Save" ng-click="saveSpaDescription()">
    <input type="submit" ng-show="delete_spa_button" class="btn btn-primary"  data-toggle="modal" data-target="#deleteModal" value="Delete" ng-click="deleteSpaDescription()"></div>
</div>


   <div class="panel panel-primary">
   <div class="panel-heading">
      <h3 class="panel-title">
         dining Description
      </h3>
   </div>
   <div class="panel-body">
      <textarea rows="8" cols="100" ng-model="dining.diningDesc" name="dining.diningDesc">
     </textarea>
   </div>
   <div class="panel-footer"> <input type="submit" class="btn btn-primary" data-toggle="modal" data-target="#saveModal" value="Save" ng-click="saveDiningDescription()">
    <input type="submit" ng-show="delete_dining_button" class="btn btn-primary" data-toggle="modal" data-target="#deleteModal" value="Delete" ng-click="deleteDiningDescription()"></div>
</div>

  <div class="panel panel-primary">
   <div class="panel-heading">
      <h3 class="panel-title">
         What to Expect
      </h3>
   </div>
   <div class="panel-body">
       <textarea rows="8" cols="100" ng-model="expect.whatToExpect" name="expect.whatToExpect">
     </textarea>
   </div>
   <div class="panel-footer"> <input type="submit" class="btn btn-primary" data-toggle="modal" data-target="#saveModal" value="Save" ng-click="saveExpectDescription()">
    <input type="submit" ng-show="delete_expect_button" class="btn btn-primary"  data-toggle="modal" data-target="#deleteModal" value="Delete" ng-click="deleteExpectDescription()"></div>
</div><br/><br/>
  
    <!-- Modal for save and delete success messages -->
  <div id="saveModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="alert alert-success alert-dismissible" role="alert">Save was Succesful</div>
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer"><button type="button" data-dismiss="modal" class="btn btn-primary">OK</button></div>
    </div>
  </div>
  </div>
    
  <div id="deleteModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="alert alert-success alert-dismissible" role="alert">Delete was Succesful</div>
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer"><button type="button" data-dismiss="modal" class="btn btn-primary">OK</button></div>
    </div>
  </div>
  </div>
  
</div>
 <div class="navbar navbar-default navbar-fixed-bottom">

     <div class="container" style="text-align:center;font-size:12px;">
     
     	<p>Copyright &copy; 2014 </p>
     	<p>Developed by Vikas Joshi & Priyanka Narendran for CS-5200</p>
     <!-- <p class="muted pull-right"> Created by Z the Man of The master Plan</p>
     <p class="muted pull-left"> Created by Z the Man of The master Plan</p>
 -->
   </div> <!-- container-->
   </div> <!-- navbar navbar-default navbar-fixed-bottom" --> 
</body>
</html>