<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html ng-app="app">
<head>
<title>Book-a-Hotel</title>
<!--  TODO: Move this links to a separate file and link them -->
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link rel="stylesheet" href="<c:url value="/resources/angucomplete-master/angucomplete.css"/>">

<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-sanitize.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-resource.min.js" />"></script>
<script
	src="<c:url value="/resources/angucomplete-master/angucomplete.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-0.11.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-transition.js" />"></script>
<script>
	var app = angular.module('app', ['angucomplete', 'ui.bootstrap']);
</script>
<script type="text/javascript">
	function AutoCompleteController($scope, $http, $window) {
		var baseUrl='http://localhost:8080/book-a-hotel/';
		  $scope.digitArray =[{"digit":"1"},{"digit":"2"},{"digit":"3"},{"digit":"4"},{"digit":"5"},
		                      {"digit":"6"},{"digit":"7"},{"digit":"8"},{"digit":"9"},{"digit":"10"}];
		  $scope.checkin = '';
		  $scope.checkout = '';
		  $scope.rooms ='';
		  $scope.adults ='';
		  $scope.adults.digit = '1';
		  $scope.children = '';
		  $scope.children.digit = '0';
		  
        $scope.add = function(city, country) {
			
        	$window.location.href = baseUrl+'Details.jsp?city='+city
        			                 +'&country='+country+'&checkin='+$scope.checkin+'&checkout='+$scope.checkout
        			                 +'&rooms='+$scope.rooms.digit+'&adults='+$scope.adults.digit+'&children='+$scope.children.digit;
          }
        
	    }
	
	    function checkinController($scope)
        {
    		  // Disable weekend selection
    		  $scope.disabled = function(date, mode) {
    		    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    		  };

    		  $scope.toggleMin = function() {
    		    $scope.minDate = $scope.minDate ? null : new Date();
    		  };
    		  $scope.toggleMin();

    		  $scope.open = function($event) {
    		    $event.preventDefault();
    		    $event.stopPropagation();

    		    $scope.opened = true;
    		  };

    		  $scope.dateOptions = {
    		    formatYear: 'yy',
    		    startingDay: 1
    		  };

    		  $scope.initDate = new Date('2016-15-20');
    		  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd'];
    		  $scope.format = $scope.formats[4];
        }
        
        
	    function checkoutController($scope)
        {
         	        	  
         		  $scope.toggleMin = function() {
         		    $scope.minDate = $scope.checkin ? null : new Date();
         		  };
         		  $scope.toggleMin();

         		  $scope.open = function($event) {
         		    $event.preventDefault();
         		    $event.stopPropagation();

         		    $scope.opened = true;
         		  };

         		  $scope.dateOptions = {
         		    formatYear: 'yy',
         		    startingDay: 1
         		  };

         		  $scope.initDate = new Date('2016-15-20');
         		  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd'];
         		  $scope.format = $scope.formats[4];
             
        }

	
</script>
</head>
<body>
<nav class="navbar navbar-inverse" style="background-color: #5CADFF;border: 0px;" role="navigation">
   <div class="container-fluid">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<%-- <ul class="nav navbar-nav">
		  <li class="active"><a href="<c:url value="/Welcome.jsp"/>" target="_self">Book a Hotel</a></li>
		</ul>
 --%>
				<span style="font-size: 28px; color: white;"><a
					href="/book-a-hotel/Welcome.jsp" target="_self"
					style="color: white;">Book a Hotel</a></span>

				<ul class="nav navbar-nav navbar-right">
		   <li class="active"><sec:authorize access="!isAuthenticated()">
                  <a href="<c:url value="/Login.jsp"/>">Login</a>
                </sec:authorize>
            </li>
    
            <li class="active">
			   <sec:authorize access="isAuthenticated()">
                 <a href="<c:url value="/History.jsp"/>" target="_self">Booking History</a>
                </sec:authorize>
		      </li>
					
               <li class="active"><sec:authorize access="isAuthenticated()">
                    <a href="<c:url value="/Logout.jsp"/>" target="_self">Logout</a>
                   </sec:authorize>
               </li>
		
			    <li class="active"><a href="<c:url value="/AboutUs.jsp"/>" target="_self">About Us</a></li>
		    </ul>
	    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
 </nav>
 <div class="container" ng-controller="AutoCompleteController">
    <div class="panel panel-primary">
       <div class="panel-body">
				<h2 style="color: white; background-color: #2E8AE6; /* text-align: center; */ padding: 5px 5px 5px 40px;">Find
					Hotel Deals</h2>
				<div style="height: 21px;"></div>
				<form class="form-horizontal" >
          <div class="form-group">
           <label class="col-xs-2 control-label">City</label>
            <div class="col-lg-5">
             <angucomplete id="ex2" placeholder="Enter City" pause="150"
				selectedobject="selectedCity"
				url="http://localhost:8080/book-a-hotel/active-property/get-cities/"
				datafield="activePropertyList"
				searchfields="city,stateProvince,country"
				titlefield="city,stateProvince,country"
				descriptionfield="search cities" minlength="3"
				inputclass="form-control form-control-small" matchclass="highlight" />
             </div>
           </div>
           
           <div class="form-group">
           <label class="col-xs-2 control-label">Arrival Date</label>
            <div class="col-lg-5" ng-controller="checkinController">
                <p class="input-group">
              <input type="text" class="form-control" required="required" datepicker-popup="{{format}}" ng-model="$parent.checkin" is-open="opened" min-date="minDate" max-date="'2015-06-22'" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" />
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
              </span>
            </p>
             </div>
           </div>
           
           
          <div class="form-group">
           <label class="col-xs-2 control-label">Departure Date</label>
            <div class="col-lg-5" ng-controller="checkoutController">
               <p class="input-group">
              <input type="text" class="form-control" required="required" datepicker-popup="{{format}}" ng-model="$parent.checkout" is-open="opened" min-date="minDate" max-date="'2015-06-22'" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" />
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
              </span>
            </p>
             </div>
            
           </div> 
           
          <div class="form-group">
            <label class="col-xs-2 control-label">Rooms</label>
            <div class="col-lg-5">
                 <select class="form-control" ng-model="rooms" name="rooms" required="required" ng-options="num.digit for num in digitArray">
              </select>
             </div>
           </div> 
           
           <div class="form-group">
            <label class="col-xs-2 control-label">Adults</label>
            <div class="col-lg-5">
             <select class="form-control" ng-model="adults" name="adults"  ng-required="required" ng-options="num.digit for num in digitArray">
             </select>
               
             </div>
           </div> 
           
            <div class="form-group">
            <label class="col-xs-2 control-label">Children</label>
            <div class="col-lg-5">
                  <select class="form-control" ng-model="children" name="children"  ng-required="required" ng-options="num.digit for num in digitArray">
              </select>
             </div>
           </div> 
                     
            <div class="form-group">
              <div class="col-xs-offset-3 col-xs-9">
                <input type="submit" type="button" value="Search" class="btn btn-primary btn-lg" ng-click="add(selectedCity.originalObject.city,
		                              selectedCity.originalObject.country)"></input>
		    </div>
		    </div>                          
          </form>
		
		</div>
	</div>
	</div>
	 <div class="navbar navbar-default navbar-fixed-bottom">

     <div class="container" style="text-align:center;font-size:12px;">
     
     	<p>Copyright &copy; 2014 </p>
     	<p>Developed by Vikas Joshi & Priyanka Narendran for CS-5200</p>
     <!-- <p class="muted pull-right"> Created by Z the Man of The master Plan</p>
     <p class="muted pull-left"> Created by Z the Man of The master Plan</p>
 -->
   </div> <!-- container-->
   </div> <!-- navbar navbar-default navbar-fixed-bottom" --> 
</body>
<!-- <footer>Footer Info</footer> -->
</html>