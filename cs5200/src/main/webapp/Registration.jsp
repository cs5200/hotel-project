<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html ng-app="app">
<head>
<title>Book-a-Hotel</title>

<!--  TODO: Move this links to a separate file and link them -->
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link rel="stylesheet" href="<c:url value="/resources/angucomplete-master/angucomplete.css"/>">

<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-sanitize.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-resource.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-0.11.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-transition.js" />"></script>

<!--  Initialize the app and bootstrap -->
<script type="text/javascript">
	var app = angular.module('app', ['ngResource']).config(function($locationProvider, $resourceProvider) {
		/* For retrieving the query parameters */
		$locationProvider.html5Mode(true);
	});
	
</script>


<!--  TODO: Move this content to a separate file -->
<script type="text/javascript">

     var baseUrl = 'http://localhost:8080/book-a-hotel/';
     
     /**
      * Controller for Registration.
      * Use http to connect the url using request parameters city and country,  retrieves information about it.
      * Bind it to the details object.
      *
      **/
	function MainController($scope, $http, $location,  $resource, $window) {
    	  $scope.customer={};
	    $scope.saveCustomer = function() {
	    	        $scope.customer.role='ROLE_USER';
	    		    // Create a new active property on save
	    		    $http.post(baseUrl+'create-customer/', $scope.customer).success(function(res) 
	    		    {
	    		  	  $scope.customer = res.customer;
	    		      $window.location.href = baseUrl+'Login.jsp';
	    		    }).error(function(res){});
	    		
	    
			}
	    } //End of save property
	    
</script>
</head>
<body >
<nav class="navbar navbar-inverse" style="background-color: #5CADFF;border: 0px;" role="navigation">
   <div class="container-fluid">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<%-- <ul class="nav navbar-nav">
		  <li class="active"><a href="<c:url value="/Welcome.jsp"/>" target="_self">Book a Hotel</a></li>
		</ul>
 --%>
				<span style="font-size: 28px; color: white;"><a
					href="/book-a-hotel/Welcome.jsp" target="_self"
					style="color: white;">Book a Hotel</a></span>


		<ul class="nav navbar-nav navbar-right">
		   <li class="active"><sec:authorize access="!isAuthenticated()">
                  <a href="<c:url value="/Login.jsp"/>">Login</a>
                </sec:authorize>
            </li>
    
            <li class="active">
			   <sec:authorize access="isAuthenticated()">
                 <a href="<c:url value="/History.jsp"/>" target="_self">Booking History</a>
                </sec:authorize>
		      </li>
					
               <li class="active"><sec:authorize access="isAuthenticated()">
                    <a href="<c:url value="/Logout.jsp"/>" target="_self">Logout</a>
                   </sec:authorize>
               </li>
		
			    <li class="active"><a href="<c:url value="/AboutUs.jsp"/>" target="_self">About Us</a></li>
		    </ul>
	    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
 </nav>
<div class="container-fluid">

    <h3> <span class="label label-primary">Customer Registration</span></h3>
<div class="row" ng-controller="MainController">
 <form class="form-horizontal" >
    <div class="form-group">
        <label class="col-xs-2 control-label">First Name</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="customer.firstName" class="form-control">
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-xs-2 control-label">Last Name</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="customer.lastName" required="required" class="form-control">
        </div>
    </div>

     <div class="form-group">
        <label class="col-xs-2 control-label">Password</label>
        <div class="col-lg-5">
            <input type="password" id="inputSuccess" ng-model="customer.password"  class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">Address1</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="customer.address1" required="required" class="form-control">
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-2 control-label">Address2</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="customer.address2" class="form-control">
        </div>
    </div>
    
   <div class="form-group">
        <label class="col-xs-2 control-label">City</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="customer.city"  required="required" class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">State</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="customer.state" required="required" class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">Country</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="customer.country" class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">Email Id</label>
        <div class="col-lg-5">
            <input type="email" id="inputSuccess"  ng-model="customer.emailId" required="required"  class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">Phone Number</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="customer.phoneNum" required="required" class="form-control">
        </div>
    </div>
   
    <div class="form-group">
        <div class="col-xs-offset-3 col-xs-9">
            <input type="submit" class="btn btn-primary" value="Register"  ng-click="saveCustomer()">
        </div>
    </div>
    
  
  </form>
</div>
</div>
 <div class="navbar navbar-default navbar-fixed-bottom">

     <div class="container" style="text-align:center;font-size:12px;">
     
     	<p>Copyright &copy; 2014 </p>
     	<p>Developed by Vikas Joshi & Priyanka Narendran for CS-5200</p>
     <!-- <p class="muted pull-right"> Created by Z the Man of The master Plan</p>
     <p class="muted pull-left"> Created by Z the Man of The master Plan</p>
 -->
   </div> <!-- container-->
   </div> <!-- navbar navbar-default navbar-fixed-bottom" --> 
</body>
</html>