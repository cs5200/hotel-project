<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html ng-app="app">
<head>
<title>Book-a-Hotel</title>

<!--  TODO: Move this links to a separate file and link them -->
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link rel="stylesheet" href="<c:url value="/resources/angucomplete-master/angucomplete.css"/>">

<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-sanitize.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-resource.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-0.11.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-transition.js" />"></script>

<!--  Initialize the app and bootstrap -->
<script type="text/javascript">
	var app = angular.module('app', ['ngResource',  'ui.bootstrap']).config(function($locationProvider, $resourceProvider) {
		/* For retrieving the query parameters */
		$locationProvider.html5Mode(true);
	});
	
</script>


<!--  TODO: Move this content to a separate file -->
<script type="text/javascript">

     var baseUrl = 'http://localhost:8080/book-a-hotel/';
     
     /**
      * Controller for Details.
      * Use http to connect the url using request parameters city and country,  retrieves information about it.
      * Bind it to the details object.
      *
      **/
	function MainController($scope, $http, $location,  $resource, $window) {
    	  $scope.reviews={};
		$scope.reviews.reservationId = $location.search()['reservationId'];

		$scope.hotelName = $location.search()['hotelName'];
		
		$scope.max = 5;
		
		$scope.saveReview = function()
		{
			// Delete the attractions
	  	    	 $http.post(baseUrl+'create-reviews/', $scope.reviews).success(function(res) {
	  		           
	  	    	  $scope.expect = '';
	  	    	  $scope.delete_expect_button = false;
		  	      $scope.create_expect = true;
	  	         }).error(function(res){$scope.delete_expect_button = false;});
		}
		
	}
     
</script>
</head>
<body >
<nav class="navbar navbar-inverse" style="background-color: #5CADFF;border: 0px;" role="navigation">
   <div class="container-fluid">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<%-- <ul class="nav navbar-nav">
		  <li class="active"><a href="<c:url value="/Welcome.jsp"/>" target="_self">Book a Hotel</a></li>
		</ul>
 --%>
				<span style="font-size: 28px; color: white;"><a
					href="/book-a-hotel/Welcome.jsp" target="_self"
					style="color: white;">Book a Hotel</a></span>


		<ul class="nav navbar-nav navbar-right">
		   <li class="active"><sec:authorize access="!isAuthenticated()">
                  <a href="<c:url value="/Login.jsp"/>">Login</a>
                </sec:authorize>
            </li>
    
            <li class="active">
			   <sec:authorize access="isAuthenticated()">
                 <a href="<c:url value="/History.jsp"/>" target="_self">Booking History</a>
                </sec:authorize>
		      </li>
					
               <li class="active"><sec:authorize access="isAuthenticated()">
                    <a href="<c:url value="/Logout.jsp"/>" target="_self">Logout</a>
                   </sec:authorize>
               </li>
		
			    <li class="active"><a href="<c:url value="/AboutUs.jsp"/>" target="_self">About Us</a></li>
		    </ul>
	    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
 </nav>
<div class="container-fluid">
 <p>
 This project deals with hotel booking and is developed for academic purpose. It is
developed for CS5200 Database Management Systems, Summer 2014 course, Northeastern University
Boston.<br/>
<b>Team Members</b>
<ul>
<li>
Vikas Joshi - joshi.vik@husky.neu.edu</li><li>
Priyanka Narendran - narendran.pri@husky.neu.edu</li></ul>
We downloaded the static content through Expedia API and uploaded the database.<br/>
We also used the same API for dynamic retrieval for checking availability and price.<br/>
<b>Goals</b>
<ul>
 <li> To help customer search for hotels in a city.</li>
<li>To provide information about the hotels.</li>
<li>Reservation/Cancellation of the rooms.</li>
<li>Provide reviews/feedback about the hotels.</li>
</ul>
<b>Technologies Used</b>
<ul>
<li>Spring MVC</li>
<li>Hibernate</li>
<li>AngularJS</li>
<li>Bootstrap</li>
<li>Maven</li>
<li>MySQL</li>
</ul>
<b>How do I get set up?</b><br/>
Download the MySQL and change the username and password in spring.xml file of resources folder.<br/>
Download Eclipse.<br/>
Register in expedia developer api <a href="http://developer.ean.com/docs/getting-started/" target="_blank">Link</a> and download the static content files.<br/>
<b>Database Configurations</b><br/>
Use the Load_Data.sql in resources folder and upload the data in database.<br/>
Run the hotel_database_v1.sql, hotel_database_v2.sql, hotel_database_v3.sql.<br/>
<b>Deployment instructions</b><br/>
Go to command line, go to the folder where you downloaded the source code.<br/>
Run the following command. mvn clean install tomcat7:run<br/>
Open your browser, type localhost:8080/book-a-hotel/Welcome.jsp<br/>
<b>Bit Bucket Repo</b><br/>
Here is the bit bucket url <a href="https://bitbucket.org/cs5200/hotel-project" target="_blank">https://bitbucket.org/cs5200/hotel-project</a>
       </p>
      </div>
      <!-- dialog buttons -->
 <div class="navbar navbar-default navbar-fixed-bottom">

     <div class="container" style="text-align:center;font-size:12px;">
     
     	<p>Copyright &copy; 2014 </p>
     	<p>Developed by Vikas Joshi & Priyanka Narendran for CS-5200</p>
     <!-- <p class="muted pull-right"> Created by Z the Man of The master Plan</p>
     <p class="muted pull-left"> Created by Z the Man of The master Plan</p>
 -->
   </div> <!-- container-->
   </div> <!-- navbar navbar-default navbar-fixed-bottom" -->       
</body>
</html>