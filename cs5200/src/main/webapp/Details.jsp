<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html ng-app="app">
<head>
<title>Book-a-Hotel</title>

<!--  TODO: Move this links to a separate file and link them -->
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link rel="stylesheet" href="<c:url value="/resources/angucomplete-master/angucomplete.css"/>">

<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-0.11.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-transition.js" />"></script>

<!--  Initialize the app and bootstrap -->
<script type="text/javascript">
	var app = angular.module('app', ['ui.bootstrap']).config(function($locationProvider) {
		/* For retrieving the query parameters */
		$locationProvider.html5Mode(true);
	});
</script>

<!--  TODO: Move this content to a separate file -->
<script type="text/javascript">

     var baseUrl = 'http://localhost:8080/book-a-hotel/';
    
     /**
      * Controller for Details.
      * Use http to connect the url using request parameters city and country,  retrieves information about it.
      * Bind it to the details object.
      *
      **/
	function DetailsController($scope, $http, $location) {
		$scope.city = $location.search()['city'];
		$scope.country = $location.search()['country'];
		
		$scope.checkin = new Date($location.search()['checkin']);
		$scope.checkout = new Date($location.search()['checkout']);
		 
		$scope.max = 5;
		$scope.isReadonly = true;
		
		/* Prepare the REST URL*/
		var url = baseUrl+'active-property/list-details?city='+ $scope.city + '&country=' + $scope.country;

		var detls = {};
		
		/* Hit the Url and get the response */
		$http.get(url).success(function(res) {
			$scope.details = res.activePropertyList;
		});
	
	}
    
	 function checkinController($scope, $location)
     {
 		  // Disable weekend selection
 		  $scope.disabled = function(date, mode) {
 		    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
 		  };

 		  $scope.toggleMin = function() {
 		    $scope.minDate = $scope.minDate ? null : new Date();
 		  };
 		  $scope.toggleMin();

 		  $scope.open = function($event) {
 		    $event.preventDefault();
 		    $event.stopPropagation();

 		    $scope.opened = true;
 		  };

 		  $scope.dateOptions = {
 		    formatYear: 'yy',
 		    startingDay: 1
 		  };
 		  
 		 $scope.change = function(){
  			  $location.search('checkin', $scope.checkin)
  	  		  }

 		  $scope.initDate = new Date('2016-15-20');
 		  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd'];
 		  $scope.format = $scope.formats[4];
     }
     
     
	 function checkoutController($scope, $location)
     {
      	        	  
      	  $scope.toggleMin = function() {
      		    $scope.minDate = $scope.checkin ? null : new Date();
      	    };
      		  $scope.toggleMin();

      		  $scope.open = function($event) {
      		    $event.preventDefault();
      		    $event.stopPropagation();

      		    $scope.opened = true;
      		  };

      		  $scope.dateOptions = {
      		    formatYear: 'yy',
      		    startingDay: 1
      		  };
      		  
      		$scope.change = function(){
   			  $location.search('checkout', $scope.checkout)
   			
   		  }

      		  $scope.initDate = new Date('2016-15-20');
      		  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd'];
      		  $scope.format = $scope.formats[4];
          
     }

	/**
	 * Controller for more details.
	 * Use http to connect the url using hotel Id and  retrieves information about it.
	 * Bind it to the respective object.
	 *
	 **/
	function MoreDetailsController($scope, $http, $sce, $modal, $log, $location) {

		/* Api parameters for retrieving the image objects */
		var expediaUrl = 'http://dev.api.ean.com/ean-services/rs/hotel/v3/info?minorRev=26&cid=55505&options=HOTEL_IMAGES&_type=JSONP&callback=JSON_CALLBACK&';
		var availUrl = 'http://dev.api.ean.com/ean-services/rs/hotel/v3/avail?minorRev=26&cid=55505&_type=JSONP&callback=JSON_CALLBACK&';
		var apiKey = '';
		var sessionId = '';
		var customerIpAddress = '';
		var customerSessionId = ''; 
		  
		/*Hack to use the properties file, only use for api properties*/
		$http.get('resources/properties/config.properties').success(function(res)
		{
			
		     apiKey = res.apiKey;
		     sessionId = res.sessionId;
		     customerIpAddress = res.customerIpAddress;
		     customerSessionId = res.customerSessionId; 
		});
		
		/* This method gets called when user clicks on 'More Details' hyperlink for a specific hotel 
		*/
		$scope.getMoreDetails = function(hotelId, hotelName) {
		
			/* Entities for that hotel */
			$scope.attractions='';
			$scope.policyDescription ='';
			$scope.recreationDescription ='';
			$scope.hotelId=hotelId;
		    $scope.hotelName=hotelName;
		
		    /* Prepare an array of hotelId, hotelName and send it to the controller */
			var modalInstance = $modal.open({
			      templateUrl: 'myModalContent.html',
			      controller: ModalInstanceCtrl,
			      size: 'lg',
			      resolve: {
			          hotel: function () {
			              return [$scope.hotelId, $scope.hotelName];
			            }
			          }
			    });

			    modalInstance.result.then(function (selectedItem) {
			      $scope.selected = selectedItem;
			    }, function () {
			      //$log.info('Modal dismissed at: ' + new Date());
			    });
			  
		}// End of getDetails
		
		/*Method gets called when Modal is opened and passing the hotel information */
		 var ModalInstanceCtrl = function ($scope, $modalInstance, $sce, $filter,$window, hotel) {
			
			 var areaUrl = baseUrl+'area-attraction?hotelId='+ hotel[0];
			 var policyUrl = baseUrl+'policy-description?hotelId='+ hotel[0];
			 var recreationUrl =baseUrl+'recreation-description?hotelId='+ hotel[0];
			 var spaUrl =baseUrl+'spa-description?hotelId='+ hotel[0];
			 var diningUrl =baseUrl+'dining-description?hotelId='+ hotel[0];
			 var expectUrl = baseUrl+'what-to-expect-description?hotelId='+hotel[0];
			 $scope.hotelName=hotel[1];
           
			    /* Get the area attractions and bind it to the object */
				$http.get(areaUrl).success(function(res) {
					 $scope.area = res.areaAttractions;
					 $scope.attractions = $sce.trustAsHtml($scope.area);
				});
							
			    /* Get policy Description and bind it to the object */
				$http.get(policyUrl).success(function(res) {
					$scope.pol = res.polDesc;
				    $scope.policyDescription = $sce.trustAsHtml($scope.pol);
				});
				
			    /* Get the recreation descriptions */
				$http.get(recreationUrl).success(function(res) {
					$scope.rec = res.recreationDesc;
				    $scope.recreationDescription = $sce.trustAsHtml($scope.rec);
				});
			    
			    
				 /* Get the spa description */
				$http.get(spaUrl).success(function(res) {
					$scope.spa = res.spaDesc;
				    $scope.spaDescription = $sce.trustAsHtml($scope.spa);
				    if($scope.spaDescription == null || $scope.spaDescription == '' )
				    {
				    	$scope.spaDescription = "NA";	
				    }
				});
				 
				 /* Get the dining description */
				$http.get(diningUrl).success(function(res) {
					$scope.dining = res.diningDesc;
				    $scope.diningDescription = $sce.trustAsHtml($scope.dining);
				});
				
				 /* Get the expect description */
				$http.get(expectUrl).success(function(res) {
					$scope.expect = res.whatToExpect;
				    $scope.expectDescription = $sce.trustAsHtml($scope.expect);
				    
				});
			    
			    /* Carousel Part of the  Hotel Images*/
				$scope.myInterval = 5000;
			    /* Prepare the API Url,
			       Note: you might want to change the properties file for yourself*/
					
				var imagesUrl = expediaUrl+'&apiKey='+apiKey+'&hotelId='+hotel[0]+'&customerIpAddress='+customerIpAddress;
				imagesUrl= imagesUrl+'&customerSessionId='+customerSessionId;
				
				/* Use the JSONP to avoid cross domain policy */
				$http.jsonp(imagesUrl).success(function(res) {
					$scope.slides = res.HotelInformationResponse.HotelImages.HotelImage;
				});
				
				
				$scope.rooms = $location.search()['rooms'];
			    $scope.checkin = $location.search()['checkin'];
			    $scope.checkout = $location.search()['checkout'];
				$scope.adults = $location.search()['adults'];
				$scope.children = $location.search()['children'];
				$scope.availability = false;

				
				var checkin = $filter('date')(new Date($scope.checkin), 'MM/dd/yyyy');
				var checkout =  $filter('date')(new Date($scope.checkout), 'MM/dd/yyyy');
				
				availUrl = availUrl+'&apiKey='+apiKey+'&hotelId='+hotel[0];//+'&customerIpAddress='+customerIpAddress+'customerSessionId='+customerSessionId;
				availUrl= availUrl+'&numberOfBedRooms='+$scope.rooms+'&Room.numberOfAdults='+$scope.adults;
				availUrl= availUrl+'&Room.numberOfChildren='+$scope.children+'&arrivalDate='+checkin+'&departureDate='+checkout;
				
				
				/* Modal Ok and Cancel button events */
			  $scope.ok = function () {

					/* Use the JSONP to avoid cross domain policy and check for availability and price. */
					$http.jsonp(availUrl).success(function(res) {
						$scope.availResponse = res.HotelRoomAvailabilityResponse;
					
						if($scope.availResponse['@size'] == 0)
						{
							$scope.availability = true;
						}
						else
					    {
							
							/* TODO: Error handling or null objects*/
							 var bedType = $scope.availResponse.HotelRoomResponse[0].roomTypeDescription;
						     var price = $scope.availResponse.HotelRoomResponse[0].RateInfos.RateInfo.ChargeableRateInfo['@total'];
						     
							 /* TODO: put customer Id here*/
							 var reservUrl = baseUrl+'Reservation.jsp?hotelId='+hotel[0]+'&hotelName='+hotel[1]+'&checkout='+checkout+'&checkin=';
							 reservUrl = reservUrl+checkin+'&adults='+$scope.adults+'&children='+$scope.children+'&bedType='+bedType+'&price='+price+'&rooms='+$scope.rooms;
							 /* Navigate to the reservation page */
							 $window.location.href = reservUrl;
						}
					}).error(function(res){$scope.availability = true;});
					
			    //$modalInstance.close();
			  };

			  $scope.cancel = function () {
			    $modalInstance.dismiss('cancel');
			  };
		  }
		
	}//End of controller
	
	
</script>
</head>
<body>
<nav class="navbar navbar-inverse" style="background-color: #5CADFF;border: 0px;" role="navigation">
   <div class="container-fluid">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<%-- <ul class="nav navbar-nav">
		  <li class="active"><a href="<c:url value="/Welcome.jsp"/>" target="_self">Book a Hotel</a></li>
		</ul>
 --%>
				<span style="font-size: 28px; color: white;"><a
					href="/book-a-hotel/Welcome.jsp" target="_self"
					style="color: white;">Book a Hotel</a></span>



		<ul class="nav navbar-nav navbar-right">
		   <li class="active"><sec:authorize access="!isAuthenticated()">
                  <a href="<c:url value="/Login.jsp"/>">Login</a>
                </sec:authorize>
            </li>
    
            <li class="active">
			   <sec:authorize access="isAuthenticated()">
                 <a href="<c:url value="/History.jsp"/>" target="_self">Booking History</a>
                </sec:authorize>
		      </li>
					
               <li class="active"><sec:authorize access="isAuthenticated()">
                    <a href="<c:url value="/Logout.jsp"/>" target="_self">Logout</a>
                   </sec:authorize>
               </li>
		
			    <li class="active"><a href="<c:url value="/AboutUs.jsp"/>" target="_self">About Us</a></li>
		    </ul>
	    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
 </nav>
<div class="container-fluid">
   <div ng-controller="DetailsController">
    <div class="row-fluid">
				<label class="col-xs-2 col-md-1  control-label"
					style="font-size: 13px; padding-top: 7px;">Arrival Date:</label>
				<div class="col-xs-2 col-md-3 " ng-controller="checkinController">
                <p class="input-group">
              <input type="text" class="form-control" required="required" datepicker-popup="{{format}}" ng-change="change()" ng-model="checkin" is-open="opened" min-date="minDate" max-date="'2015-06-22'" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" />
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
              </span>
            </p>
             </div>
          
          <label class="col-xs-2 col-md-1  control-label"
					style="font-size: 13px; padding-top: 7px;">Departure Date:</label>
            <div class="col-xs-2 col-md-3" ng-controller="checkoutController">
               <p class="input-group">
              <input type="text" class="form-control" required="required" datepicker-popup="{{format}}" ng-change="change()" ng-model="checkout" is-open="opened" min-date="minDate" max-date="'2015-06-22'" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" />
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
              </span>
            </p>
             </div>  
               
             <label class="col-xs-2 col-md-1  control-label"
					style="font-size: 13px; padding-top: 7px;">Filter By:</label>
             <div class="col-xs-2 col-md-3">
                     <select class="form-control" ng-model="selectedFilter">
                  <option value="-starRating">Rating</option>
                  <option value="lowRate">Low Price</option>
                  <option value="-highRate">High Price</option>
               </select>
             </div>     
    </div>  <br/><br/><br/>
 
  <div ng-repeat="detail in details| orderBy:selectedFilter">
	<div class="panel panel-default">
	  <div class="panel-body">
	    <!--  Address Info -->
	    <p>
		<span class="label label-info"> {{detail.name}} </span><br />
		  {{detail.address1}}, {{detail.city}}, {{detail.country}}<br />
		  <rating ng-model="detail.starRating" max="max"
				  readonly="isReadonly" on-hover="hoveringOver(value)"
				  on-leave="overStar = null"></rating><br />
		   Low Price: $ {{detail.lowRate}}<br /> 
		   High Price: $ {{detail.highRate}}
	
		</p>

        <!--  Controller providing more details for each hotel -->
	    <div ng-controller="MoreDetailsController">
		<a href ng-click="detail.hotelId_1 = !detail.hotelId_1;getMoreDetails(detail.hotelId, detail.name)">More Details</a>

         <!-- Modal part, TODO: move it to myModalContent.html file -->
	    <script type="text/ng-template" id="myModalContent.html">
          <div class="modal-header">
            <h3 class="modal-title"> {{hotelName}}</h3>
          </div>
          <div class="modal-body">  
          <div ng-show="availability" class="alert alert-danger alert-dismissible" role="alert">Sorry, this hotel is not available
           for your arrival and departure dates.
          </div> 
			<h3>Area Attractions:</h3><br />
		    <p ng-bind-html="attractions"></p>
			<h3>Recreation Description:</h3> <br />
			<p ng-bind-html="recreationDescription"></p>
            <h3>Dining Description:</h3><br />
			<p ng-bind-html="diningDescription"></p>
            <h3>Policy Description:</h3><br />
			<p ng-bind-html="policyDescription"></p>
             <h3>What to Expect:</h3><br />
			<p ng-bind-html="expectDescription"></p>

            <div style="height: 405px">
              <carousel interval="myInterval">
                <slide ng-repeat="slide in slides" active="slide.active">
                <img ng-src="{{slide.url}}" style="margin:auto; width:350px; height:350px; min-height:350px;">
                <div class="carousel-caption">
                  <p>{{slide.caption}}</p>
                </div>
               </slide>
              </carousel>
            </div>
		  </div>
 
        <div class="modal-footer">
          <button class="btn btn-primary" ng-click="ok()">Book Now</button>
          <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        </div> 
      </script>
	 </div> <!--  MoreDetailsController -->
	</div> <!-- Panel body -->
   </div> <!-- Panel -->
   </div> <!-- repeat -->
  </div> <!-- controller -->
  </div>
 <div class="navbar navbar-default navbar-fixed-bottom">

     <div class="container" style="text-align:center;font-size:12px;">
     
     	<p>Copyright &copy; 2014 </p>
     	<p>Developed by Vikas Joshi & Priyanka Narendran for CS-5200</p>
     <!-- <p class="muted pull-right"> Created by Z the Man of The master Plan</p>
     <p class="muted pull-left"> Created by Z the Man of The master Plan</p>
 -->
   </div> <!-- container-->
   </div> <!-- navbar navbar-default navbar-fixed-bottom" --> 
</body>
</html>