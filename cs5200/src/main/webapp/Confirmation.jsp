<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html ng-app="app">
<head>
<title>Book-a-Hotel</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  TODO: Move this links to a separate file and link them -->
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link rel="stylesheet" href="<c:url value="/resources/angucomplete-master/angucomplete.css"/>">


<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-sanitize.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-resource.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-0.11.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-transition.js" />"></script>

<!--  Initialize the app and bootstrap -->
<script type="text/javascript">
	var app = angular.module('app', ['ngResource']).config(function($locationProvider, $resourceProvider) {
		/* For retrieving the query parameters */
		$locationProvider.html5Mode(true);
	});
	
</script>


<!--  TODO: Move this content to a separate file -->
<script type="text/javascript">

     var baseUrl = 'http://localhost:8080/book-a-hotel/';
     
     /**
      * Controller for Details.
      * Use http to connect the url using request parameters city and country,  retrieves information about it.
      * Bind it to the details object.
      *
      **/
	function MainController($scope, $http, $location,  $resource, $window) {
    		
    		$scope.reservation='';
		    $scope.cancelled = 'NO'; 			
            $scope.reservationId =  $location.search()['reservationId'];  
            $scope.isCancelledDisabled = false;
            
            // If reservationId exists, get the details
		   	   $http.get(baseUrl+'reservation?reservationId='+$scope.reservationId).success(function(res) {
		  		       $scope.reservation = res;
		  		     var checkoutDate = new Date($scope.reservation.checkoutDate);
		  			 var today = new Date();
		  			
		  			    if(checkoutDate < today)
		  			    {
		  			 	   $scope.isCancelledDisabled = true;
		  			     }
		  			    
		  		     $http.get(baseUrl+'admin/get-active-property/'+ $scope.reservation.hotelId).success(function(res) {
		  
		  		    	 $scope.hotelName = res.name;
		  			}).error(function(res){$scope.delete_cancel_button = false;});
		  		     
		  		     
		  		     $scope.delete_cancel_button = true;
		  	     }).error(function(res){ $scope.delete_cancel_button = false;});
         
            $scope.cancelReservation = function(){
            	
            	// If reservationId exists, update the cancelled column
	  		  	 $http.post(baseUrl+'update-reservation/'+$scope.reservationId, $scope.reservation).success(function(res) {
	  		     	$scope.reservation = res;
	  		  		       
	  		  	   if($scope.reservation.isCancelled)
	  		    	 {
	  		  			   $scope.cancelled = 'YES'; 	
	  		  			   $scope.delete_cancel_button = false;
	  		  	 	 }
	  		  	  	}).error(function(res){ $scope.delete_cancel_button = true;});
             }	    
	}
     
</script>
</head>
<body >
 <nav class="navbar navbar-inverse" style="background-color: #5CADFF;border: 0px;" role="navigation">
   <div class="container-fluid">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<%-- <ul class="nav navbar-nav">
		  <li class="active"><a href="<c:url value="/Welcome.jsp"/>" target="_self">Book a Hotel</a></li>
		</ul>
 --%>
				<span style="font-size: 28px; color: white;"><a
					href="/book-a-hotel/Welcome.jsp" target="_self"
					style="color: white;">Book a Hotel</a></span>



		<ul class="nav navbar-nav navbar-right">
		   <li class="active"><sec:authorize access="!isAuthenticated()">
                  <a href="<c:url value="/Login.jsp"/>">Login</a>
                </sec:authorize>
            </li>
    
            <li class="active">
			   <sec:authorize access="isAuthenticated()">
                 <a href="<c:url value="/History.jsp"/>" target="_self">Booking History</a>
                </sec:authorize>
		      </li>
					
               <li class="active"><sec:authorize access="isAuthenticated()">
                    <a href="<c:url value="/Logout.jsp"/>" target="_self">Logout</a>
                   </sec:authorize>
               </li>
		
			    <li class="active"><a href="<c:url value="/AboutUs.jsp"/>" target="_self">About Us</a></li>
		    </ul>
	    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
 </nav>

<div class="container-fluid" ng-controller="MainController">
  <div class="row-fluid">
  <div class="col-md-8">
  <div class="panel panel-primary">
      <div class="panel-heading">Thank you for using Book-a-Hotel.com.Here is your summary of the Hotel Booking.
</div>
      <div class="panel-body">
      <label class="control-label" style="min-width: 171px;">Reservation Number:</label>RES{{reservation.reservationId}}<br/>
      <label class="control-label" style="min-width: 171px;">Hotel:</label>{{ hotelName }}<br/>
      <label class="control-label" style="min-width: 171px;">Checkin Date:</label>{{ reservation.checkinDate }}<br/>
      <label class="control-label" style="min-width: 171px;">Checkout Date:</label>{{ reservation.checkoutDate }}<br/>
      <label class="control-label" style="min-width: 171px;">Number of rooms:</label>{{ reservation.rooms }}<br/>
      <label class="control-label" style="min-width: 171px;">Total Price:</label>$ {{ reservation.price }}</br>
      <label class="control-label" style="min-width: 171px;">Cancelled Reservation:</label> {{cancelled}}</br>
      </div>
  </div>
         <!-- <p><b>
       
      Hotel: {{ hotelName }}<br/>
      Guest Name: {{ reservation.firstName }} {{ reservation.lastName }}<br/>
      Checkin Date: {{ reservation.checkinDate }}<br/>
      checkout Date: {{ reservation.checkoutDate }}<br/>
      Number of rooms: {{ reservation.rooms }}<br/>
      Price:$ {{ reservation.price }}</b><br/>
      <b> </b>
      </p> -->
       <input type="submit" ng-show="delete_cancel_button" ng-disabled="isCancelledDisabled" class="btn btn-primary" data-toggle="modal" data-target="#saveModal" value="Cancel my Reservation" ng-click="cancelReservation()">
  </div>
       
        <!-- Modal for save and delete success messages -->
  <div id="saveModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="alert alert-success alert-dismissible" role="alert">Your Reservation has been Cancelled Successfully</div>
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer"><button type="button" data-dismiss="modal" class="btn btn-primary">OK</button></div>
    </div>
  </div>
  </div>
  
   </div><!-- row -->
  </div><!-- container -->
 <div class="navbar navbar-default navbar-fixed-bottom">

     <div class="container" style="text-align:center;font-size:12px;">
     
     	<p>Copyright &copy; 2014 </p>
     	<p>Developed by Vikas Joshi & Priyanka Narendran for CS-5200</p>
     <!-- <p class="muted pull-right"> Created by Z the Man of The master Plan</p>
     <p class="muted pull-left"> Created by Z the Man of The master Plan</p>
 -->
   </div> <!-- container-->
   </div> <!-- navbar navbar-default navbar-fixed-bottom" --> 
</body>
</html>