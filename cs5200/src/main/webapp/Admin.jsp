<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html ng-app="app">
<head>
<title>Book-a-Hotel</title>

<!--  TODO: Move this links to a separate file and link them -->
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link rel="stylesheet" href="<c:url value="/resources/angucomplete-master/angucomplete.css"/>">

<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-sanitize.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-resource.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-0.11.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-transition.js" />"></script>

<!--  Initialize the app and bootstrap -->
<script type="text/javascript">
	var app = angular.module('app', ['ngResource']).config(function($locationProvider, $resourceProvider) {
		/* For retrieving the query parameters */
		$locationProvider.html5Mode(true);
	});
	
</script>


<!--  TODO: Move this content to a separate file -->
<script type="text/javascript">

     var baseUrl = 'http://localhost:8080/book-a-hotel/';
     
     /**
      * Controller for Details.
      * Use http to connect the url using request parameters city and country,  retrieves information about it.
      * Bind it to the details object.
      *
      **/
	function MainController($scope, $http, $location,  $resource, $window) {
		$scope.hotelId = $location.search()['hotelId'];
		$scope.delete_button = false;		
		
		if (typeof $scope.hotelId  != 'undefined')
	    {
		/* Prepare the REST URL*/
		var url = baseUrl+'admin/get-active-property/'+ $scope.hotelId;

		var detls = {};
		$scope.delete_button = true;
		/* Hit the Url and get the response */
		$http.get(url).success(function(res) {
			
			if(res == null || res == '' || res.hotelId == null || res.hotelId== '')
			{
				$scope.delete_button = false;
			}
			$scope.activeProperty = res;
		}).error(function(res){$scope.delete_button = false;});
	  }
	
	    $scope.saveActiveProperty = function() {
	    	
	    	// Check for not null of hotelId, if it is not specified create a new object
	    	if(typeof $scope.hotelId  == 'undefined' || $scope.hotelId == null || $scope.hotelId == '')
	    		{
	    		
	    		    $scope.delete_button = false;
	    		    // Create a new active property on save
	    		    $http.post(baseUrl+'admin/create-active-property/', $scope.activeProperty).success(function(res) 
	    		    {
	    		  	  $scope.activeProperty = res;
	    		  	  $scope.hotelId = $scope.activeProperty.hotelId;
	    		  	  $location.search('hotelId', $scope.hotelId);
	    		  	  $scope.delete_button = true;
	    		    }).error(function(res){$scope.delete_button = false;});
	    		}
	    	else
	    		{
	    		   // If hotelId exists, update the active property at the backend
	    	       $http.post(baseUrl+'admin/update-active-property/'+$scope.hotelId, $scope.activeProperty).success(function(res) {
		           $scope.activeProperty = res;
	    	     }).error(function(res){$scope.delete_button = false;});
			}
	    } //End of save property
	    
	    
	       $scope.deleteActiveProperty = function() {
	    	if(typeof $scope.hotelId  == 'undefined' || $scope.hotelId == null || $scope.hotelId == '')
	    		{
	    		  $scope.delete_button = false;
	    		}
	    	else
	    		{
	    		   // Delete the property
	    	       $http.post(baseUrl+'admin/delete-active-property/'+$scope.hotelId, $scope.activeProperty).success(function(res) {
		           
	    	       $scope.activeProperty = '';
	    	       $scope.hotelId = '';
	    	       $location.url($location.path());
	    	       $scope.delete_button = false;
	    	     }).error(function(res){$scope.delete_button = false;});
			}
	    }//End of delete property
	    
	    
	    $scope.editDescriptions = function() {
	    	if(typeof $scope.hotelId  == 'undefined' || $scope.hotelId == null || $scope.hotelId == '')
	    		{
	    		  $scope.delete_button = false;
	    		}
	    	else
	    		{
	    		  $window.location.href = 'http://localhost:8080/book-a-hotel/Descriptions.jsp?hotelId='+$scope.hotelId;
			   }
	    }//End of edit property
	}
     
</script>
</head>
<body >
<nav class="navbar navbar-inverse" style="background-color: #5CADFF;border: 0px;" role="navigation">
   <div class="container-fluid">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<%-- <ul class="nav navbar-nav">
		  <li class="active"><a href="<c:url value="/Welcome.jsp"/>" target="_self">Book a Hotel</a></li>
		</ul>
 --%>
				<span style="font-size: 28px; color: white;"><a
					href="/book-a-hotel/Welcome.jsp" target="_self"
					style="color: white;">Book a Hotel</a></span>


		<ul class="nav navbar-nav navbar-right">
		   <li class="active"><sec:authorize access="!isAuthenticated()">
                  <a href="<c:url value="/Login.jsp"/>">Login</a>
                </sec:authorize>
            </li>
    
            <li class="active">
			   <sec:authorize access="isAuthenticated()">
                 <a href="<c:url value="/History.jsp"/>" target="_self">Booking History</a>
                </sec:authorize>
		      </li>
					
               <li class="active"><sec:authorize access="isAuthenticated()">
                    <a href="<c:url value="/Logout.jsp"/>" target="_self">Logout</a>
                   </sec:authorize>
               </li>
		
			    <li class="active"><a href="<c:url value="/AboutUs.jsp"/>" target="_self">About Us</a></li>
		    </ul>
	    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
 </nav>
<div class="container-fluid">

    
<div class="row" ng-controller="MainController">
<h3 style="/* text-align:center; */padding-bottom: 20px;padding-left: 20px;"> Active Property Details </h3>
 <form class="form-horizontal" >
    <div class="form-group">
        <label class="col-xs-2 control-label">Hotel Id</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.hotelId"   readonly="true" class="form-control">
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-xs-2 control-label">Name <span style="color:red">* </span></label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.name" required="required" class="form-control">
        </div>
    </div>

     <div class="form-group">
        <label class="col-xs-2 control-label">Address1<span style="color:red">* </span></label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.address1" required="required" class="form-control">
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-2 control-label">Address2</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.address2" class="form-control">
        </div>
    </div>
    
   <div class="form-group">
        <label class="col-xs-2 control-label">Location<span style="color:red">* </span></label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.location"  required="required" class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">City <span style="color:red">* </span> </label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.city" required="required" class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">State Province</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.stateProvince" class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">Postal Code</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess"  ng-model="activeProperty.postalCode" class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">Country <span style="color:red">* </span> </label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.country" required="required" class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">Latitude</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.latitude" class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">Longitude</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.longitude"  class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">Airport Code <span style="color:red">* </span> </label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.airportCode" required="required" class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">Property Category</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.propertyCategory" class="form-control" >
        </div>
    </div>
    
      <div class="form-group">
        <label class="col-xs-2 control-label">Property Currency</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.propertyCurrency" class="form-control" >
        </div>
    </div>
    
      <div class="form-group">
        <label class="col-xs-2 control-label">Star Rating</label>
        <div class="col-lg-5">
        <!--  TODO: Use dropdown and bind the value -->
            <input type="text" id="inputSuccess"  ng-model="activeProperty.starRating" class="form-control">
            <!-- <select class="form-control" ng-model="activeProperty.starRating">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select> -->
        </div>
    </div>
    
    
      <div class="form-group">
        <label class="col-xs-2 control-label">Confidence</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.confidence" class="form-control">
        </div>
    </div>
    
      <div class="form-group">
        <label class="col-xs-2 control-label">Supplier Type</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.supplierType" class="form-control">
        </div>
    </div>
    
    
      <div class="form-group">
        <label class="col-xs-2 control-label">Chain Code Id</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.chainCodeId" class="form-control">
        </div>
    </div>
    
      <div class="form-group">
        <label class="col-xs-2 control-label">Region Id</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.regionId" class="form-control">
        </div>
    </div>
    
      <div class="form-group">
        <label class="col-xs-2 control-label">High Rate</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.highRate" class="form-control">
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-xs-2 control-label">Low Rate</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.lowRate" class="form-control">
        </div>
    </div>
    
     <div class="form-group">
        <label class="col-xs-2 control-label">Checkin Time</label>
        <div class="col-lg-5">
            <input type="text" id="inputSuccess" ng-model="activeProperty.checkinTime" class="form-control">
        </div>
     </div>
    
    <div class="form-group">
      <label class="col-xs-2 control-label">Checkout Time</label>
      <div class="col-lg-5">
          <input type="text" id="inputSuccess" ng-model="activeProperty.checkoutTime" class="form-control">
       </div>
    </div>

    <div class="form-group">
        <div class="col-xs-offset-3 col-xs-9">
            <input type="submit" class="btn btn-primary" value="Save"  data-toggle="modal" data-target="#saveModal" ng-click="saveActiveProperty()">
            <input type="submit" ng-show="delete_button" class="btn btn-primary"  data-toggle="modal" data-target="#deleteModal" value="Delete" ng-click="deleteActiveProperty()">
             <input type="submit" ng-show="delete_button" class="btn btn-primary"  value="Edit Descriptions" ng-click="editDescriptions()">
        </div>
    </div>
    
  <div id="saveModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="alert alert-success alert-dismissible" role="alert">Save was Succesful</div>
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer"><button type="button" data-dismiss="modal" class="btn btn-primary">OK</button></div>
    </div>
  </div>
  </div>
  
  <div id="deleteModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- dialog body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="alert alert-success alert-dismissible" role="alert">Delete was Succesful</div>
      </div>
      <!-- dialog buttons -->
      <div class="modal-footer"><button type="button" data-dismiss="modal" class="btn btn-primary">OK</button></div>
    </div>
  </div>
  </div>
  
  </form>
</div>
</div><br/><br/><br/>
 <div class="navbar navbar-default navbar-fixed-bottom">
     <div class="container" style="text-align:center;font-size:12px;">
     
     	<p>Copyright &copy; 2014 </p>
     	<p>Developed by Vikas Joshi & Priyanka Narendran for CS-5200</p> 
     <!-- <p class="muted pull-right"> Created by Z the Man of The master Plan</p>
     <p class="muted pull-left"> Created by Z the Man of The master Plan</p>
 -->
   </div> <!-- container-->
   </div> <!-- navbar navbar-default navbar-fixed-bottom" --> 
</body>
</html>