/****************************************************************
 Description: Hotel database for CS 5200 project.               *
             Creating table for payment and adding some         *
             constraints on customer table.                     *
 Team Members: 1. Vikas Joshi                                   *
               2. Priyanka Narendran                            *
 Date: 07/30/2014                                               *
 Version: 3.0                                                   *       
*****************************************************************/
 -- Add Indexes

USE hotel;
ALTER TABLE `customer` ADD CONSTRAINT customer_unqiue UNIQUE(`email_id`);

ALTER TABLE `reservation` ADD `first_name` varchar(100) NOT NULL;
ALTER TABLE `reservation` ADD `last_name` varchar(100) NOT NULL;
ALTER TABLE `reservation` ADD `email_id` varchar(100) NOT NULL;
ALTER TABLE `reservation` ADD `phone_number` varchar(20) NOT NULL;


DROP TABLE IF EXISTS `payment_details`;
CREATE TABLE `payment_details`(
`reservation_id` int NOT NULL PRIMARY KEY,
`cardholder_name` varchar(100) NOT NULL,
`card_number` varchar(20) NOT NULL,
`exp_month` varchar(10) NOT NULL,
`exp_year` int NOT NULL,
`security_code` int NOT NULL,
`country` varchar(10) NOT NULL,
`email_address` varchar(100) NOT NULL,
`billing_address1` varchar(300) NOT NULL,
`billing_address2` varchar(300) NOT NULL,
`city` varchar(100) NOT NULL,
`postal_code` varchar(20) NOT NULL,
 FOREIGN KEY (`reservation_id`) REFERENCES 
  `reservation`(`reservation_id`)
)ENGINE=Innodb charset=utf8;



ALTER TABLE `reservation` DROP PRIMARY KEY, ADD PRIMARY KEY(`reservation_id`);
ALTER TABLE `reservation` CHANGE `booking_date` `checkin_date` DATE NOT NULL;
ALTER TABLE `reservation` ADD `checkout_date` DATE NOT NULL;
ALTER TABLE `reservation` CHANGE `customer_id` `customer_id` INT NULL;
ALTER TABLE `reservation` CHANGE `checkin_date` `checkin_date` varchar(20) NOT NULL;
ALTER TABLE `reservation` CHANGE `checkout_date` `checkout_date` varchar(20) NOT NULL;
ALTER TABLE `reservation` ADD `adults` int NOT NULL;
ALTER TABLE `reservation` ADD `children` int DEFAULT 0;
ALTER TABLE `reservation` ADD `bed_type` varchar(200);


ALTER TABLE `customer` DROP `SSN`;
ALTER TABLE `customer` ADD `password` varchar(50) NOT NULL;
ALTER TABLE `customer` CHANGE `address` `address1` varchar(50) NOT NULL;
ALTER TABLE `customer` ADD `address2` varchar(50);

ALTER TABLE `payment_details` CHANGE `billing_address2`  `billing_address2` varchar(300);



ALTER TABLE `reviews` DROP `review_title`;
ALTER TABLE `reviews` ADD `reservation_id` int NOT NULL;
ALTER TABLE `reviews` DROP FOREIGN KEY `reviews_ibfk_1`;
ALTER TABLE `reviews` DROP FOREIGN KEY `reviews_ibfk_2`;
ALTER TABLE `reviews` DROP `customer_id`;
ALTER TABLE `reviews` DROP `hotel_id`;
ALTER TABLE `reviews` DROP `reservation_id`;
ALTER TABLE `reviews` ADD `reservation_id` int NOT NULL REFERENCES `reservation`(`reservation_id`);
ALTER TABLE `reviews` CHANGE `review_date` `review_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `reviews` ADD constraint uc_res_id UNIQUE(`reservation_id`);
ALTER TABLE `reviews` ADD `hotel_id` int NOT NULL REFERENCES `active_property`(`hotel_id`);



ALTER TABLE `customer` ADD column `role` varchar(30) DEFAULT 'ROLE_USER';