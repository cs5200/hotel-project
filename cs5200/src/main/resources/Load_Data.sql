/**
 Responsible for loading the static data in the hotel database.
 Author: Vikas Joshi.
 Date: 7/2/2014
*/
-- Replace the F:\\MS\\DB to your directory path

USE `hotel`;

-- Better to use truncate, explore how to use truncate cascade
DELETE FROM active_property;
LOAD DATA infile 'F:\\MS\\DB\\project\\ActivePropertyList.txt' 
INTO TABLE hotel.active_property  FIELDS 
TERMINATED BY '|' LINES TERMINATED BY '\n'
IGNORE 1 LINES;

SET foreign_key_checks = 0;
DELETE FROM policy_description;
LOAD DATA infile 'F:\\MS\\DB\\project\\PolicyDescriptionList.txt' 
INTO TABLE hotel.policy_description  FIELDS 
TERMINATED BY '|' LINES TERMINATED BY '\n'
IGNORE 1 LINES;


DELETE FROM recreation_description;
LOAD DATA infile 'F:\\MS\\DB\\project\\RecreationDescriptionList.txt' 
INTO TABLE hotel.recreation_description  FIELDS 
TERMINATED BY '|' LINES TERMINATED BY '\n'
IGNORE 1 LINES;

DELETE FROM property_description;
LOAD DATA infile 'F:\\MS\\DB\\project\\PropertyTypeList.txt' 
INTO TABLE hotel.property_description  FIELDS 
TERMINATED BY '|' LINES TERMINATED BY '\n'
IGNORE 1 LINES;

DELETE FROM area_attraction;
LOAD DATA infile 'F:\\MS\\DB\\project\\AreaAttractionsList.txt' 
INTO TABLE hotel.area_attraction  FIELDS 
TERMINATED BY '|' LINES TERMINATED BY '\n'
IGNORE 1 LINES;

DELETE FROM what_to_expect_description;
LOAD DATA infile 'F:\\MS\\DB\\project\\WhatToExpectList.txt' 
INTO TABLE hotel.what_to_expect_description  FIELDS 
TERMINATED BY '|' LINES TERMINATED BY '\n'
IGNORE 1 LINES;

DELETE FROM spa_description;
LOAD DATA infile 'F:\\MS\\DB\\project\\SpaDescriptionList.txt' 
INTO TABLE hotel.spa_description  FIELDS 
TERMINATED BY '|' LINES TERMINATED BY '\n'
IGNORE 1 LINES;

DELETE FROM dining_description;
LOAD DATA infile 'F:\\MS\\DB\\project\\DiningDescriptionLIst.txt' 
INTO TABLE hotel.dining_description  FIELDS 
TERMINATED BY '|' LINES TERMINATED BY '\n'
IGNORE 1 LINES;

COMMIT;
