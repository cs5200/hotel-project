/****************************************************************
 Description: Hotel database for CS 5200 project.               *
 Team Members: 1. Vikas Joshi                                   *
               2. Priyanka Narendran                            *
 Date: 06/27/2014                                               *
 Version: 1.0                                                   *
 Version: 1.2 -- Addind on delete cascade constraints           *
*****************************************************************/
 -- Create Database
DROP DATABASE IF EXISTS hotel;
CREATE DATABASE IF NOT EXISTS `hotel`;

USE hotel;

-- 1. Active property list table
DROP TABLE IF EXISTS `active_property`;
CREATE TABLE `active_property`
(
  `hotel_id`        int,
  `sq_number`       int,
  `name`            varchar(70),
  `address1`        varchar(50),
  `address2`        varchar(50),
  `city`            varchar(50),
  `state_province`  varchar(2),
  `postal_code`     varchar(15),
  `country`         varchar(2),
  `latitude`        decimal(8,5),
  `longitude`       decimal(8,5),
  `airport_code`    varchar(3),
  `property_category` int,
    `property_currency` varchar(3),
  `star_rating`     decimal(2,1),
  `confidence`      int,
  `supplier_type`   varchar(3),
  `location`        varchar(80),
  `chain_code_id`   varchar(5),
  `region_id`       int,
  `high_rate`       decimal(19,4),
  `low_rate`        decimal(19,4),
  `checkin_time`    varchar(10),
  `checkout_time`   varchar(10),
  PRIMARY KEY(`hotel_id`, `sq_number`)
)ENGINE=Innodb charset=utf8;

-- 2. Property Description List
DROP TABLE IF EXISTS `property_description`;
CREATE TABLE `property_description`
(
 `hotel_id`               int NOT NULL,
 `language_code`          varchar(5),
 `property_category_desc` varchar(256),
  PRIMARY KEY(`hotel_id`),
  FOREIGN KEY (`hotel_id`) REFERENCES 
  `active_property`(`hotel_id`) ON DELETE CASCADE
)ENGINE=Innodb charset=utf8;

-- 3. Recreation Description List
DROP TABLE IF EXISTS `recreation_description`;
CREATE TABLE `recreation_description`
(
 `hotel_id`                int NOT NULL,
 `language_code`           varchar(5),
 `rec_desc`                text,
  PRIMARY KEY(`hotel_id`),
  FOREIGN KEY (`hotel_id`) REFERENCES 
  `active_property`(`hotel_id`) ON DELETE CASCADE
)ENGINE=Innodb charset=utf8;

-- 4. Policy Description List
DROP TABLE IF EXISTS `policy_description`;
CREATE TABLE `policy_description`
(
 `hotel_id`           int NOT NULL,
 `language_code`      varchar(5),
 `pol_desc`           text,
  PRIMARY KEY(`hotel_id`),
  FOREIGN KEY (`hotel_id`) REFERENCES 
  `active_property`(`hotel_id`) ON DELETE CASCADE
)ENGINE=Innodb charset=utf8;

-- 5. Area Attraction List
DROP TABLE IF EXISTS `area_attraction`;
CREATE TABLE `area_attraction`
(
 `hotel_id`           int NOT NULL,
 `language_code`      varchar(5),
 `area_attractions`   text,
  PRIMARY KEY(`hotel_id`),
  FOREIGN KEY (`hotel_id`) REFERENCES 
  `active_property`(`hotel_id`) ON DELETE CASCADE
)ENGINE=Innodb charset=utf8;

-- 6. Whattoexpect List
DROP TABLE IF EXISTS `what_to_expect_description`;
CREATE TABLE `what_to_expect_description`
(
 `hotel_id`			int NOT NULL,
 `language_code`	varchar(5),
 `what_to_expect`	text,
  PRIMARY KEY(`hotel_id`),
  FOREIGN KEY (`hotel_id`) REFERENCES 
  `active_property`(`hotel_id`) ON DELETE CASCADE
)ENGINE=Innodb charset=utf8;

-- 7. Spa Description List
DROP TABLE IF EXISTS `spa_description`;
CREATE TABLE `spa_description`
(
 `hotel_id`			int NOT NULL,
 `language_code`	varchar(5),
 `spa_desc`			text,
  PRIMARY KEY(`hotel_id`),
  FOREIGN KEY (`hotel_id`) REFERENCES 
  `active_property`(`hotel_id`) ON DELETE CASCADE
)ENGINE=Innodb charset=utf8;

-- 8. Dining Description List
DROP TABLE IF EXISTS `dining_description`;
CREATE TABLE `dining_description`
(
 `hotel_id`			int NOT NULL,
 `language_code`	varchar(5),
 `dining_desc`		text,
  PRIMARY KEY(`hotel_id`),
  FOREIGN KEY (`hotel_id`) REFERENCES 
  `active_property`(`hotel_id`) ON DELETE CASCADE
)ENGINE=Innodb charset=utf8;

-- 9. Customer
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` 
(
 `customer_id` int NOT NULL auto_increment PRIMARY KEY,
 `first_name`  varchar(100) NOT NULL,
 `last_name`   varchar(100) NOT NULL,
 `SSN`         varchar(10),
 `address`     text,
 `city`        varchar(20) NOT NULL,
 `state`       varchar(20) NOT NULL,
 `country`     varchar(20) NOT NULL,
 `email_id`    varchar(50) NOT NULL,
 `phone_num`   varchar(20)
)ENGINE=Innodb charset=utf8;
CREATE INDEX `cust_ind` ON `customer`(`email_id`);

-- 10. Reservation
DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation`
(
 `reservation_id` int NOT NULL auto_increment PRIMARY KEY,
 `customer_id`    int NOT NULL REFERENCES `customer`(`customer_id`) ON DELETE CASCADE,
 `hotel_id`       int NOT NULL REFERENCES `active_property`(`hotel_id`) ON DELETE CASCADE,
 `booking_date`   date,
 `rooms`		  int NOT NULL,
 `price`		  double NOT NULL,
 `is_cancelled`   boolean DEFAULT false,
  FOREIGN KEY (`hotel_id`) REFERENCES 
  `active_property`(`hotel_id`),
  FOREIGN KEY (`customer_id`) REFERENCES 
  `customer`(`customer_id`)
)ENGINE=Innodb charset=utf8;
CREATE INDEX `reser_ind` ON `reservation`(`customer_id`, `hotel_id`);

-- 11. Reviews
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews`
(
 `review_number`  int NOT NULL auto_increment PRIMARY KEY,
 `customer_id`    int NOT NULL REFERENCES `customer`(`customer_id`) ON DELETE CASCADE,
 `hotel_id`       int NOT NULL REFERENCES `active_property`(`hotel_id`) ON DELETE CASCADE,
 `review_date`	  date,
 `review_title`	  varchar(100) NOT NULL,
 `review`		  text,
 `no_of_votes`	  int DEFAULT 0,
 `rating`		  int NOT NULL, 
  FOREIGN KEY (`hotel_id`) REFERENCES 
  `active_property`(`hotel_id`),
  FOREIGN KEY (`customer_id`) REFERENCES 
  `customer`(`customer_id`),
  CHECK (rating<6)
)ENGINE=Innodb charset=utf8;
CREATE INDEX `review_ind` ON `reviews`(`hotel_id`, `review_date`);

/*************************END*******************************/