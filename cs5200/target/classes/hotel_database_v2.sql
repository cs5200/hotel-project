/****************************************************************
 Description: Hotel database for CS 5200 project.               *
              Adding index and making autoserial on the active  *
              property table.                                   *
 Team Members: 1. Vikas Joshi                                   *
               2. Priyanka Narendran                            *
 Date: 07/27/2014                                               *
 Version: 2.0                                                   *       
*****************************************************************/
 -- Add Indexes

USE hotel;

ALTER TABLE `active_property` ADD INDEX `city_index` (`city`);
ALTER TABLE `active_property` DROP PRIMARY KEY, ADD PRIMARY KEY(`hotel_id`);
ALTER TABLE `active_property` CHANGE `hotel_id` `hotel_id` INT(10) AUTO_INCREMENT ;
ALTER TABLE `active_property` CHANGE  COLUMN `sq_number` `sq_number` INT NOT NULL DEFAULT 1;