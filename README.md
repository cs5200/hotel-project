# README #

* This project deals with hotel booking and is developed for academic purpose. It is 

  developed for CS5200 Database Management Systems, Summer 2014 course, Northeastern University 

  Boston. 

  Team Members:

  1. Vikas Joshi - joshi.vik@husky.neu.edu

  2. Priyanka Narendran - narendran.pri@husky.neu.edu
  
  We downloaded the static content through Expedia API and uploaded the database.
  
  We also used the same API for dynamic retrieval for checking availability and price.
  

### How do I get set up? ###

* Summary of set up
   Download the MySQL and change the username and password in spring.xml file of resources 
   folder.
   
   Download Eclipse.
   
   Register in expedia developer api and download the static content files.

* Database Configurations
   
   Use the Load_Data.sql in resources folder and upload the data in database.
   
   Run the hotel_database_v1.sql, hotel_database_v2.sql, hotel_database_v3.sql.

* Deployment instructions
  
  Go to command line, go to the folder where you downloaded the source code. 
  
  Run the following command.
  mvn clean install tomcat7:run
  
  open your browser, type localhost:8080/book-a-hotel/Welcome.jsp